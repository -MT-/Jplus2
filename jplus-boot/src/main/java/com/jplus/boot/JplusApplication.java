package com.jplus.boot;

import com.jplus.core.core.AnnotationConfigApplicationContext;
import com.jplus.core.core.ApplicationContext;
import com.jplus.core.mvc.AnnotationConfigWebApplicationContext;
import com.jplus.core.utill.clazz.ClassUtil;

public class JplusApplication {
	private static final String WEB_ENVIRONMENT_CLASSES = "javax.servlet.Servlet";
	private Object[] sources;
	private ApplicationContext context;

	public static ApplicationContext run(Object source, String... args) {
		return run(new Object[] { source }, args);
	}

	public static ApplicationContext run(Object[] source, String... args) {
		return new JplusApplication(source).run(args);
	}

	// ============================================
	public JplusApplication(Object[] sources) {
		initialize(sources);
	}

	private void initialize(Object[] sources) {
		if (ClassUtil.hasClass(WEB_ENVIRONMENT_CLASSES)) {
			context = new AnnotationConfigWebApplicationContext();
		} else {
			context = new AnnotationConfigApplicationContext();
		}
	}

	private ApplicationContext run(String... args) {
		return null;

	}
}
