package com.jplus.plugin.rpc.process;

import java.nio.ByteBuffer;

import com.jplus.plugin.rpc.codec.NIODecoder;
import com.jplus.plugin.rpc.nio.NIO.NIORole;

public class StringRpcProcess extends NIOProcess {

	public StringRpcProcess(NIORole role, NIODecoder decoder) {
		super(role, decoder);
	}

	@Override
	protected ByteBuffer execute(ByteBuffer buffer) {
		byte[] data = decoder.decoder(buffer);
		if (data != null) {
			switch (role) {
			case CLIENT:
				System.out.println(new String(data));
				break;
			case SERVER:
				String str = (new String(data));
				str += "hello world";
				return ByteBuffer.wrap(str.getBytes());
			// break;
			}
		}
		return null;
	}

}
