package com.jplus.plugin.rpc.process;

import java.nio.ByteBuffer;

import com.jplus.core.utill.JSON;
import com.jplus.core.utill.JSON.JSONObject;
import com.jplus.plugin.rpc.codec.NIODecoder;
import com.jplus.plugin.rpc.nio.NIO.NIORole;

public class JsonRpcProcess extends NIOProcess {

	public JsonRpcProcess(NIORole role, NIODecoder decoder) {
		super(role, decoder);
	}

	@Override
	protected ByteBuffer execute(ByteBuffer buffer) {
		buffer.flip();
		byte[] bytes = new byte[buffer.remaining()];
		buffer.get(bytes);
		buffer.clear();
		switch (role) {
		case CLIENT:
			System.out.println(new String(bytes));
			break;
		case SERVER:
			JSONObject jobj = JSON.parseObject(new String(bytes));
			jobj.put("RPC_SERVER", "hello world" + System.currentTimeMillis());
			buffer.put(jobj.toJSONString().getBytes());
			queue.add(buffer);
			break;
		}
		return buffer;
	}

}
