package com.jplus.plugin.rpc.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.plugin.rpc.process.StringRpcProcess;
import com.jplus.plugin.rpc.codec.LineDecoder;
import com.jplus.plugin.rpc.process.NIOProcess;

/**
 *
 */
public class NIOServer extends NIO {
	final Logger log = LoggerFactory.getLogger(NIOServer.class);

	public static void main(String[] args) throws IOException {
		System.out.println("NIOServer");
		new NIOServer("127.0.0.1", 8088, new StringRpcProcess(NIORole.SERVER, new LineDecoder())).start();
	}

	public NIOServer(String host, int port, NIOProcess process) throws IOException {
		super.process = process;
		super.threadPool = Executors.newFixedThreadPool(5);
		// ServerSocketChannel
		ServerSocketChannel ssc = ServerSocketChannel.open();
		ssc.configureBlocking(false);
		ssc.bind(new InetSocketAddress(host, port));
		// selector
		super.selector = Selector.open();
		ssc.register(selector, SelectionKey.OP_ACCEPT);
		System.out.println("OK");
	}

}