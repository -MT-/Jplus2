package com.jplus.plugin.rpc.codec;

import java.nio.ByteBuffer;

/**
 * 行拆包器 LineBasedFrameDecoder
 * 
 * 每个应用层数据包，都以换行符作为分隔符，进行分割拆分。
 * 
 * @author Yuanqy
 *
 */
public class LineDecoder extends NIODecoder {

	@Override
	public byte[] decoder(ByteBuffer buffer) {
		buffer.flip();
		byte[] bytes = new byte[buffer.remaining()];
		buffer.get(bytes);
		buffer.clear();
		// ========================

		for (int i = 0; i < bytes.length; i++) {
			byte b = bytes[i];
			if (b == '\n') {
				super.queue.add(ByteBuffer.wrap(bytes, 0, i));
				
				break;
			}

		}

		return null;
	}

}
