package com.jplus.plugin.rpc.codec;

import java.nio.ByteBuffer;

/**
 * 基于数据包长度的拆包器 LengthFieldBasedFrameDecoder
 * 
 * 将应用层数据包的长度，作为接收端应用层数据包的拆分依据。按照应用层数据包的大小，拆包。
 * 
 * <pre>
 * AFTER DECODE (15 bytes)          
 * +------+--------+----------------+
 * | Head | Length |Content(12bytes)|
 * | 0xF1 | 0x000C | "HELLO, WORLD" |
 * +------+--------+----------------+
 * </pre>
 * 
 * LineBasedFrameDecoder
 * 
 * @author Yuanqy
 *
 */
public class LengthFieldDecoder extends NIODecoder {

	@Override
	public byte[] decoder(ByteBuffer buffer) {
		// TODO Auto-generated method stub
		return null;
	}

}
