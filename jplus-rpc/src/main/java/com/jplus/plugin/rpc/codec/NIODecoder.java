package com.jplus.plugin.rpc.codec;

import java.nio.ByteBuffer;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 应对接受端,数据粘包,拆包的问题
 * 
 * @author Yuanqy
 *
 */
public abstract class NIODecoder {
	protected final ConcurrentLinkedQueue<ByteBuffer> queue = new ConcurrentLinkedQueue<>();

	public abstract byte[] decoder(ByteBuffer buffer);
}
