package com.jplus.core.junit.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jplus.core.anno.boot.Component;

/**
 * 项目配置文件路径
 * 
 * @author Yuanqy
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface ContextConfiguration {
	/**
	 * 加载配置文件,具体加载规则见：AbstractConfiguration.java
	 */
	// String locations() default "";

	Class<?>[] classes();

}
