package com.jplus.core.junit;

import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

import com.jplus.core.core.AnnotationConfigApplicationContext;
import com.jplus.core.junit.annotation.ContextConfiguration;

/**
 * Junit4扩展
 * 
 * @author Yuanqy
 */
public class JUnit4Runner extends BlockJUnit4ClassRunner {
	private Object obj = null;// 测试类容器实例

	public JUnit4Runner(Class<?> cls) throws InitializationError {
		super(cls);
		ContextConfiguration cc = cls.getAnnotation(ContextConfiguration.class);
		if (cc != null) {
			AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(cc.classes());// 初始化ApplicationContext容器
			context.register(cls);
			obj = context.getBean(cls); // 获取测试类实例
		}
	}

	@Override
	protected Statement methodInvoker(FrameworkMethod method, Object test) {
		System.err.println(">>>>>>>>>>>>>>>> Junit4Runner Starting >>>>>>>>>>>>>>>>>>>> ");
		return super.methodInvoker(method, obj);// 丢弃原始对象，使用容器对象
	}
}
