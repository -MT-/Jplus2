package com.jplus.core.utill;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * TTLCache
 */
public class TTLCache<K, V> extends ConcurrentHashMap<K, V> {

    private static final long serialVersionUID = 1L;
    private static Lock lk = new ReentrantLock();

    private LinkedTransferQueue<CacheTag<K>> queue = new LinkedTransferQueue<>();
    private Integer ttl = 1000;// 单位：毫秒
    private Thread thread;

    public TTLCache(Integer ttl) {
        this.ttl = ttl;
        AutoClear();
    }

    @Override
    public V put(K key, V value) {
        V res = super.put(key, value);
        addTag(key, value);
        return res;
    }

    private void addTag(K key, V value) {
        queue.add(new CacheTag<K>(key, new Date().getTime(), value.hashCode()));
    }

    private void AutoClear() {
        thread = new Thread("TTLCache-Auto") {
            @Override
            public void run() {
                try {
                    while (true) {
                        CacheTag<K> tag = queue.take();
                        long exp = tag.time + ttl;
                        long now = new Date().getTime();
                        if (exp > now)
                            Thread.sleep(exp - now);
                        V value = get(tag.key);
                        if (value != null && value.hashCode() == tag.hashCode)
                            remove(tag.key);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.setDaemon(true); // 设置守护线程
        thread.start();
    }

    private class CacheTag<K> {
        public K key;
        public Long time;
        public int hashCode;

        public CacheTag(K key, Long time, int hashCode) {
            this.key = key;
            this.time = time;
            this.hashCode = hashCode;
        }

    }

    /**
     * 获取缓存。如果不存在，则重载
     * 
     * <pre>
     * String value = cache.get(key, (o1) -> {
     *     // example
     *     return redis.get(o1);
     * });
     * </pre>
     */
    public V get(K key, Reload<K, V> reload) {
        V value = get(key);
        if (value == null) {
            lk.lock();// 排它锁
            try {
                if (value == null)// 二次校验
                    value = reload.get(key);
                if (value != null)
                    put(key, value);
            } finally {
                lk.unlock();
            }
        }
        return value;
    }

    /** 重载最新数据 */
    public static interface Reload<K, V> {
        V get(K o1);
    }
}