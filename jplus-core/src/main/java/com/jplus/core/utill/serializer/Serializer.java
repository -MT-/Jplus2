package com.jplus.core.utill.serializer;

import java.io.IOException;

/**
 * 对象序列化接口
 */
public interface Serializer {

	public byte[] serialize(Object obj) throws IOException;

	public Object deserialize(byte[] bytes) throws IOException;

}
