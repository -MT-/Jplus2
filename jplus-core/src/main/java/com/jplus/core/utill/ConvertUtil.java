package com.jplus.core.utill;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 【非单例】 类型转换器
 * 
 * @author Yuanqy
 *
 */
public abstract class ConvertUtil {
	private String DEFFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	public static ConvertUtil newInstance() {
		return new ConvertUtil() {
			@Override
			public <T> T custom(String str, Class<T> type) {
				return null;
			}

			@Override
			public String custom(Object obj) {
				return null;
			}
		};
	}

	public void setDefDateFormat(String dateFormat) {
		this.DEFFAULT_DATE_FORMAT = dateFormat;
	}

	public <T> T convert(Object obj, Class<T> type) {
		if (obj == null)
			return convert(null, type);
		else
			return convert(obj.toString(), type);
	}

	/**
	 * String 转 基本对象
	 */
	@SuppressWarnings("unchecked")
	public <T> T convert(String str, Class<T> type) {
		T t = custom(str, type); // 优先自定义
		if (t != null)
			return t;
		if (str != null) {
			if (Integer.class.isAssignableFrom(type) || type.equals(int.class)) {
				return (T) new Integer(str);
			} else if (Long.class.isAssignableFrom(type) || type.equals(long.class)) {
				return (T) new Long(str);
			} else if (Boolean.class.isAssignableFrom(type) || type.equals(boolean.class)) {
				return (T) new Boolean(str);
			} else if (Short.class.isAssignableFrom(type) || type.equals(short.class)) {
				return (T) new Short(str);
			} else if (Float.class.isAssignableFrom(type) || type.equals(float.class)) {
				return (T) new Float(str);
			} else if (Double.class.isAssignableFrom(type) || type.equals(double.class)) {
				return (T) new Double(str);
			} else if (Byte.class.isAssignableFrom(type) || type.equals(byte.class)) {
				return (T) new Byte(str);
			} else if (Character.class.isAssignableFrom(type) || type.equals(char.class)) {
				return (T) new Character(str.charAt(0));
			} else if (String.class.isAssignableFrom(type)) {
				return (T) str;
			} else if (BigDecimal.class.isAssignableFrom(type)) {
				return (T) new BigDecimal(str);
			} else if (Date.class.isAssignableFrom(type)) {
				SimpleDateFormat formatter = new SimpleDateFormat(DEFFAULT_DATE_FORMAT);
				try {
					return (T) formatter.parse(str);
				} catch (ParseException e) {
					throw new RuntimeException(e);
				}
			} else if (type.isEnum()) {
				@SuppressWarnings("rawtypes")
				Class<? extends Enum> encla = (Class<? extends Enum>) type;
				return (T) Enum.valueOf(encla, str);
			}
		} else {
			if (type.equals(int.class)) {
				return (T) new Integer(0);
			} else if (type.equals(long.class)) {
				return (T) new Long(0L);
			} else if (type.equals(boolean.class)) {
				return (T) new Boolean(false);
			} else if (type.equals(short.class)) {
				return (T) new Short("0");
			} else if (type.equals(float.class)) {
				return (T) new Float(0.0);
			} else if (type.equals(double.class)) {
				return (T) new Double(0.0);
			} else if (type.equals(byte.class)) {
				return (T) new Byte("0");
			} else if (type.equals(char.class)) {
				return (T) new Character('\u0000');
			}
		}
		return null;
	}

	/**
	 * String 转基本对象
	 */
	public abstract <T> T custom(String str, Class<T> type);

	/**
	 * 对象转String
	 */
	public abstract String custom(Object obj);

}
