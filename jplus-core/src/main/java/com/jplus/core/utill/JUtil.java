package com.jplus.core.utill;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 基本工具类
 * 
 * @author Yuanqy
 *
 */
public final class JUtil {

	/** ==============IS Base=================== */
	/** 判断是否为整数(包括负数) */
	public static boolean isNumber(Object arg) {
		return NumberBo(0, (arg));
	}

	/** 判断是否为小数(包括整数,包括负数) */
	public static boolean isDecimal(Object arg) {
		return NumberBo(1, (arg));
	}

	/** 判断是否为空 ,为空返回true */
	public static boolean isEmpty(Object arg) {
		return toString(arg).length() == 0 ? true : false;
	}

	public static boolean isNotEmpty(Object arg) {
		return !isEmpty(arg);
	}

	/** ==============TO Base=================== */
	/**
	 * Object 转换成 Int 转换失败 返回默认值 0 <br>
	 * 使用:toInt(值,默认值[选填])
	 */
	public static int toInt(Object... args) {
		int def = 0;
		if (args != null) {
			String str = toString(args[0]);
			// 判断小数情况。舍弃小数位
			// int stri = str.indexOf('.');
			// str = stri > 0 ? str.substring(0, stri) : str;
			if (args.length > 1)
				def = Integer.parseInt(args[args.length - 1].toString());
			if (isNumber(str))
				return Integer.parseInt(str);
		}
		return def;
	}

	/**
	 * Object 转换成 Long 转换失败 返回默认值 0 <br>
	 * 使用:toLong(值,默认值[选填])
	 */
	public static long toLong(Object... args) {
		Long def = 0L;
		if (args != null) {
			String str = toString(args[0]);
			if (args.length > 1)
				def = Long.parseLong(args[args.length - 1].toString());
			if (isNumber(str))
				return Long.parseLong(str);
		}
		return def;
	}

	/**
	 * Object 转换成 Double 转换失败 返回默认值 0 <br>
	 * 使用:toDouble(值,默认值[选填])
	 */
	public static double toDouble(Object... args) {
		double def = 0;
		if (args != null) {
			String str = toString(args[0]);
			if (args.length > 1)
				def = Double.parseDouble(args[args.length - 1].toString());
			if (isDecimal(str))
				return Double.parseDouble(str);
		}
		return def;
	}

	/**
	 * Object 转换成 BigDecimal 转换失败 返回默认值 0 <br>
	 * 使用:toDecimal(值,默认值[选填]) <br>
	 * 【特别注意】: new BigDecimal(Double) 会有误差，得先转String <br>
	 * 【特别注意】: 由于精度问题，不能涉及到其他数据类型 :float,double..浮点数类型<br>
	 * 【特别注意】：涉及到金额的承载及操作，不允许用浮点数！！
	 */
	public static BigDecimal toDecimal(Object... args) {
		BigDecimal def = BigDecimal.ZERO;
		if (args != null) {
			String str = toString(args[0]);
			if (args.length > 1)
				def = new BigDecimal(args[args.length - 1].toString());
			if (isDecimal(str))
				return new BigDecimal(str);
		}
		return def;
	}

	/**
	 * Object 转换成 Boolean 转换失败 返回默认值 false <br>
	 * 使用:toBoolean(值,默认值[选填])<br>
	 * true=[1,true,"true","ok","yes","y"]<br>
	 */
	public static boolean toBoolean(Object bo) {
		String bool = toString(bo, "").toUpperCase();
		if (bool.equals("1") || bool.equals("TRUE") || bool.equals("OK") || bool.equals("YES") || bool.equals("Y"))
			return true;
		return false;
	}

	/**
	 * Object 转换成 String 为null 返回空字符 <br>
	 * 使用:toString(值,默认值[选填])
	 */
	public static String toString(Object... args) {
		String def = "";
		if (args != null) {
			Object obj = args[0];
			if (args.length > 1)
				def = toString(args[args.length - 1]);
			if (obj == null)
				return def;
			return obj.toString();
		} else {
			return def;
		}
	}

	/**
	 * Object 转换成 String[去除所以空格]; 为null 返回空字符 <br>
	 * 使用:toStringTrim(值,默认值[选填])
	 */
	public static String toStringTrimAll(Object... args) {
		return trimAll(toString(args));
	}

	/** ==============Other Base=================== */

	public static String getNowTime() {
		return getTime("yyyy-MM-dd HH:mm:ss", new Date());
	}

	public static String getNowTime(String format) {
		return getTime(format, new Date());
	}

	public static String getTime(String format, Date date) {
		return new SimpleDateFormat(format).format(date);
	}

	/**
	 * 获取对象，如果为Null，则返回默认值
	 */
	public static <T> T notNullDef(T obj, T def) {
		if (obj == null)
			return def;
		return obj;
	}

	/** 字符左边补0 ,obj:要补0的数， length:补0后的长度 */
	public static String leftPad(Object obj, int len, String def) {
		String param = toString(obj);
		int l = param.length();
		if (len < l)
			return param;
		return getPadStr(len - l, def) + param;
	}

	/** 获取 len长度的def字符串 */
	public static String getPadStr(int len, String def) {
		String ret = "";
		for (int i = 0; i < len; i++)
			ret += def;
		return ret;
	}

	/** 小数 转 百分数 */
	public static String toPercent(Double str) {
		StringBuffer sb = new StringBuffer(Double.toString(str * 100.0000d));
		return sb.append("%").toString();
	}

	/** 百分数 转 小数 */
	public static Double toPercent2(String str) {
		if (str.charAt(str.length() - 1) == '%')
			return Double.parseDouble(str.substring(0, str.length() - 1)) / 100.0000d;
		return 0d;
	}

	/**
	 * 将byte[] 转换成16进制字符串
	 */
	public static String byte2Hex(byte[] srcBytes) {
		StringBuilder hexRetSB = new StringBuilder();
		for (byte b : srcBytes)
			hexRetSB.append(byte2Hex(b));
		return hexRetSB.toString();
	}

	public static String byte2Hex(byte srcByte) {
		return String.format("%02X", srcByte);
	}

	public static byte hex2Byte(String source) {
		return (byte) Integer.parseInt(source, 16);
	}

	/**
	 * 将16进制字符串转为byte[]
	 */
	public static byte[] hex2Bytes(String source) {
		byte[] sourceBytes = new byte[source.length() / 2];
		for (int i = 0; i < sourceBytes.length; i++)
			sourceBytes[i] = hex2Byte(source.substring(i * 2, i * 2 + 2));
		return sourceBytes;
	}

	/** 获取字符串str 左边len位数 */
	public static String getLeft(Object obj, int len) {
		String str = toString(obj);
		if (len <= 0)
			return "";
		if (str.length() <= len)
			return str;
		else
			return str.substring(0, len);
	}

	/** 获取字符串str 右边len位数 */
	public static String getRight(Object obj, int len) {
		String str = toString(obj);
		if (len <= 0)
			return "";
		if (str.length() <= len)
			return str;
		else
			return str.substring(str.length() - len, str.length());
	}

	/**
	 * 首字母变小写
	 */
	public static String firstCharToLowerCase(String str) {
		Character firstChar = str.charAt(0);
		String tail = str.substring(1);
		str = Character.toLowerCase(firstChar) + tail;
		return str;
	}

	/**
	 * 首字母变大写
	 */
	public static String firstCharToUpperCase(String str) {
		Character firstChar = str.charAt(0);
		String tail = str.substring(1);
		str = Character.toUpperCase(firstChar) + tail;
		return str;
	}

	/**
	 * 字符串格式化<br>
	 * 使用占位符：{}
	 */
	public static String formatParams(String message, Object... params) {
		StringBuffer msg = new StringBuffer();
		String temp = "";
		for (int i = 0; i < params.length + 1; i++) {
			int j = message.indexOf("{}") + 2;
			if (j > 1) {
				temp = message.substring(0, j);
				temp = temp.replace("{}", JUtil.toString(params[i]));
				msg.append(temp);
				message = message.substring(j);
			} else {
				msg.append(message);
				message = "";
			}
		}
		return msg.toString();
	}

	/**
	 * 字符串格式化<br>
	 * 使用占位符：${xxx} <br>
	 * xxx 为你的Map<key>
	 */
	public static String formatParams(String message, Map<String, String> params) {
		for (Map.Entry<String, String> entry : params.entrySet())
			message = message.replaceAll("\\$\\{" + entry.getKey() + "\\}", entry.getValue());
		return message;
	}

	/**
	 * 获取随机数
	 * 
	 * @param [min <= return < max]
	 */
	public static int getRandomInt(int min, int max) {
		return min + (int) (Math.random() * (max - min));
	}

	/** ============== END =================== */

	private static boolean NumberBo(int type, Object obj) {
		if (isEmpty(obj))
			return false;
		int points = 0;
		int chr = 0;
		String str = toString(obj);
		for (int i = str.length(); --i >= 0;) {
			chr = str.charAt(i);
			if (chr < 48 || chr > 57) { // 判断数字
				if (i == 0 && chr == 45) // 判断 - 号
					return true;
				if (i >= 0 && chr == 46 && type == 1) { // 判断 . 号
					++points;
					if (points <= 1)
						continue;
				}
				return false;
			}
		}
		return true;
	}

	/**
	 * List拆分成多个
	 */
	public static <T> List<List<T>> splitList(List<T> targe, int size) {
		List<List<T>> listArr = new ArrayList<List<T>>();
		// 获取被拆分的数组个数
		int arrSize = targe.size() % size == 0 ? targe.size() / size : targe.size() / size + 1;
		for (int i = 0; i < arrSize; i++) {
			List<T> sub = new ArrayList<T>();
			// 把指定索引数据放入到list中
			for (int j = i * size; j <= size * (i + 1) - 1; j++) {
				if (j <= targe.size() - 1) {
					sub.add(targe.get(j));
				}
			}
			listArr.add(sub);
		}
		return listArr;
	}

	/**
	 * 去除所有空白字符
	 */
	public static String trimAll(String str) {
		return str.replaceAll("\\s*", "");
	}

	/** 格式化路径中的空白字符和斜杠 */
	public static String formatPath(String path) {
		path = path.replaceAll("\\\\", "/");
		path = path.replaceAll("/+", "/");
		path = JUtil.trimAll(path);
		return path;
	}

	/**
	 * 拼接字符串
	 */
	public static String join(String[] str, String chares) {
		Assert.notEmpty(str);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < str.length; i++) {
			sb.append(str[i]);
			if (i < str.length - 1)
				sb.append(chares);
		}
		return sb.toString();
	}

}