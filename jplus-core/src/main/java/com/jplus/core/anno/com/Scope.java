package com.jplus.core.anno.com;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Bean实例作用域
 * 
 * @author Yuanqy
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Scope {

	/**
	 * TODO 后续完善扩展：SINGLETON单例、PROTOTYPE多例、REQUEST线程内单例、SESSION、
	 */
	public static enum EScope {
		SINGLETON, PROTOTYPE, REQUEST
	}

	EScope value() default EScope.SINGLETON;
}
