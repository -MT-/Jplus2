package com.jplus.core.anno.boot;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 加载配置文件默认规则，当然你也可以自定义规则,举例：<br>
 * 
 * <pre>
     * "file:/xxx/app.properties"           //系统根目录
     * "xxx/app.properties"                 //项目根目录
     * "/xxx/app.properties"                //项目WEB-INF下
     * "classpath:xxx/app.properties"       //项目ClassPath下
 * </pre>
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PropertySource {

	String[] value();

	String encoding() default "UTF-8";

}