package com.jplus.core.anno.boot;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jplus.core.plugin.ImportSelector;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Import {

	/**
	 * 导入两种对象。 {@link Configuration}, {@link ImportSelector},
	 */
	Class<?>[] value();

}