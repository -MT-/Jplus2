package com.jplus.core.fault;

/**
 * 初始化错误
 *
 * @author huangyong
 */
public class InitializationError extends Error {

	private static final long serialVersionUID = -3787545505955441913L;

	public InitializationError() {
        super();
    }

    public InitializationError(String message) {
        super(message);
    }

    public InitializationError(String message, Throwable cause) {
        super(message, cause);
    }

    public InitializationError(Throwable cause) {
        super(cause);
    }
}
