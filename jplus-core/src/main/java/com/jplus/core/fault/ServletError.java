/**
 * 
 */
package com.jplus.core.fault;

/**
 * @author yuanqy
 *
 */
@SuppressWarnings("serial")
public class ServletError extends RuntimeException {
	public int code;

	public ServletError(int code, String message) {
		super(message);
		this.code = code;
	}

	public static class Error400 extends ServletError {
		public Error400(String message) {
			super(400, "[400 Bad Request]" + message);
		}
	}

	public static class Error403 extends ServletError {
		public Error403(String message) {
			super(403, "[403 Forbidden]" + message);
		}
	}

	public static class Error404 extends ServletError {
		public Error404(String message) {
			super(404, "[404 Not found]" + message);
		}
	}

	public static class Error500 extends ServletError {
		public Error500(String message) {
			super(500, "[500 Internal Server Error]" + message);
		}
	}
}
