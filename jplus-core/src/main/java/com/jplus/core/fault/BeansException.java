/**
 * 
 */
package com.jplus.core.fault;

/**
 * @author yuanqy
 *
 */
public class BeansException extends RuntimeException {

	private static final long serialVersionUID = 6198544749465871912L;
	public BeansException() {
		super();
	}

	public BeansException(String message) {
		super(message);
	}

	public BeansException(String message, Throwable cause) {
		super(message, cause);
	}

	public BeansException(Throwable cause) {
		super(cause);
	}
}
