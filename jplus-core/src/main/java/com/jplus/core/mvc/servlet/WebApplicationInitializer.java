package com.jplus.core.mvc.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * 【核心】web启动
 * 
 * @author Yuanqy
 *
 */
public interface WebApplicationInitializer {

	/**
	 * Configure the given {@link ServletContext} with any servlets, filters,
	 * listeners context-params and attributes necessary for initializing this
	 * web application. See examples {@linkplain WebApplicationInitializer
	 * above}.
	 * 
	 * @param servletContext
	 *            the {@code ServletContext} to initialize
	 * @throws ServletException
	 *             if any call against the given {@code ServletContext} throws a
	 *             {@code ServletException}
	 */
	void onStartup(ServletContext servletContext) throws ServletException;

}