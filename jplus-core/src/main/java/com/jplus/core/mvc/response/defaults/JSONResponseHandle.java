package com.jplus.core.mvc.response.defaults;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jplus.core.core.ApplicationContext;
import com.jplus.core.mvc.response.ResponseHandle;
import com.jplus.core.utill.JSON;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 默认的JSON 处理器，可替换成其他
 * 
 * @author Yuanqy
 */
public class JSONResponseHandle extends ResponseHandle {
	private final Logger log = LoggerFactory.getLogger(JSONResponseHandle.class);

	public JSONResponseHandle() {
		super.registerBody(this);
	}

	@Override
	public void handle(String requestPath, Object result, HttpServletRequest req, HttpServletResponse rep,
			ApplicationContext context) {
		try {
			String rs = JSON.toJSONString(result);
			rep.setContentType("application/json;charset=utf-8");
			rep.getWriter().write(rs);
		} catch (IOException e) {
			log.error("JSONResponseHandle ERROR:", e);
		}
	}

}
