package com.jplus.core.mvc.vo;

/**
 * @author yuanqy
 */
public class View {

    public enum TYPE {
        FORWARD, REDIRECT
    }

    private String path; // 视图路径
    private TYPE type;// 跳转类型：forward/redirect

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public View(String path, TYPE type) {
        this.path = path;
        this.type = type;
    }


    public View() {
        super();
    }

}
