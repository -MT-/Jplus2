package com.jplus.core.mvc.response;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jplus.core.core.ApplicationContext;
import com.jplus.core.mvc.response.defaults.JSONResponseHandle;
import com.jplus.core.mvc.response.defaults.JSPResponseHandle;

/**
 * 这是解析器基类接口。框架默认只提供两种实现返回：<br>
 * 
 * 如果你想换种 解析器，可以自己写个类，继承ResponseHandle接口， <br>
 * 除了默认的两种方式，你还可以指定任意类型的解析器，在Controller方法上加上@ResponseBody注解，在里面指定解析器就可以了。
 * 任意解析器都可以配置，比如：freemarker、velocity 等等
 */
public abstract class ResponseHandle {

	// 默认body解析器，可修改
	private static ResponseHandle defResponseBody;
	// 默认view解析器，可修改
	private static ResponseHandle defResponseView;

	static {
		// -- 系统默认支持 --
		new JSONResponseHandle();
		new JSPResponseHandle();
	}

	/**
	 * 数据/视图 处理
	 */
	public abstract void handle(String requestUri, Object result, HttpServletRequest req, HttpServletResponse rep,
			ApplicationContext context);

	public static ResponseHandle getDefResponseBody() {
		return defResponseBody;
	}

	public static ResponseHandle getDefResponseView() {
		return defResponseView;
	}

	/**
	 * 注册默认内容解析器
	 */
	protected void registerBody(ResponseHandle handle) {
		defResponseBody = handle;
	}

	/**
	 * 注册默认视图解析器
	 */
	protected void registerView(ResponseHandle handle) {
		defResponseView = handle;
	}

}
