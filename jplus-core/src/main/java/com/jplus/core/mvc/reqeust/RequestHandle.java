package com.jplus.core.mvc.reqeust;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jplus.core.core.ApplicationContext;
import com.jplus.core.mvc.WebContext;
import com.jplus.core.mvc.reqeust.defaults.FormReqeustHandle;
import com.jplus.core.mvc.reqeust.defaults.JsonRequestHandle;
import com.jplus.core.utill.JUtil;

public abstract class RequestHandle {

	private static final Map<String, RequestHandle> maps = new ConcurrentHashMap<String, RequestHandle>();

	static {
		// -- 系统默认支持 --
		new FormReqeustHandle();
		new JsonRequestHandle();
	}

	public static RequestHandle getHandle(String contentType) {
		if (JUtil.isEmpty(contentType))
			return maps.get(ContentTypeEnum.APPFORM.type);
		return maps.get(contentType);
	}

	protected void register(String contentType, RequestHandle handle) {
		maps.put(contentType, handle);
	}

	protected Object getHttpServlet(Class<?> cla) {
		if (cla.equals(HttpServletRequest.class))
			return WebContext.getRequest();
		else if (cla.equals(HttpServletResponse.class))
			return WebContext.getResponse();
		else if (cla.equals(ServletContext.class))
			return WebContext.getServletContext();
		else if (cla.equals(HttpSession.class))
			return WebContext.getSession();
		return null;
	}

	/**
	 * 请求入参构建
	 */

	public abstract Object[] buildRequstParam(Method method, ApplicationContext context);
}
