package com.jplus.core.mvc.reqeust;

public enum ContentTypeEnum {
	ALL("*/*"),
	TEXTHTML("text/html"), 
	TEXTPLAIN("text/plain"), 
	TEXTXML("text/xml"), 
	
	IMAGEGIF("image/gif"),
	IMAGEJPEG("image/jpeg"), 
	IMAGEPNG("image/png"), 
	
	APPXHTML("application/xhtml+xml"), 
	APPXML("application/xml"),
	APPATOM("application/atom+xml"), 
	APPJSON("application/json"), 
	APPPDF("application/pdf"),
	APPMSWORD("application/msword"), 
	APPSTREAM("application/octet-stream"),
	APPFORM("application/x-www-form-urlencoded"), 
	
	FORMDATA("multipart/form-data");

	public String type;

	private ContentTypeEnum(String type) {
		this.type = type;
	}
}