package com.jplus.core.mvc.vo;

import java.util.Date;
import java.util.HashMap;

/**
 * 封装返回数据
 * 
 * @author yuanqy
 * @since 1.0
 */
public class Model extends HashMap<String, Object> {

	private static final long serialVersionUID = 4536248880955736013L;

	private final int SUCC = 1;// 成功
	private final int FAIL = -1;// 失败

	private final String code = "code";
	private final String data = "data";
	private final String time = "time";

	/**
	 * 默认
	 */
	public Model() {
		put(this.time, new Date());
	}

	public Model SUCC() {
		return CODE(SUCC);
	}

	public Model FAIL() {
		return CODE(FAIL);
	}

	public Model CODE(int code) {
		put(this.code, code);
		return this;
	}

	public Model DATA(Object data) {
		put(this.data, data);
		return this;
	}

	public int getCode() {
		return (int) get(this.code);
	}

	public void setCode(int code) {
		put(this.code, code);
	}

	public Object getData() {
		return get(this.data);
	}

	public void setData(Object data) {
		put(this.data, data);
	}

	public Date getTime() {
		return (Date) get(this.time);
	}

	public Boolean isSUCC() {
		return (int) get(this.code) == SUCC;
	}

}
