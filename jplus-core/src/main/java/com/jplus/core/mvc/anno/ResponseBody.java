package com.jplus.core.mvc.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jplus.core.mvc.response.ResponseHandle;

/**
 * ResponseBody 跟随解析器走
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResponseBody {

	/**
	 * 自定义解析器实现
	 */
	Class<? extends ResponseHandle> handle() default ResponseHandle.class;
}
