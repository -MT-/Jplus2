package com.jplus.core.mvc.reqeust;

public enum RequestMethod {

    ALL, GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE;

    public static RequestMethod getMethod(String method) {
        RequestMethod m = RequestMethod.valueOf(method);
        if (m == null) return RequestMethod.ALL;
        return m;
    }

}
