package com.jplus.core.mvc.vo;

/**
 * 封装视图对象
 * 
 * @author Yuanqy
 */
public class ModelAndView {

	private Model model = new Model();;
	private View view = new View();

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

	public void putAttr(String key, Object value) {
		model.put(key, value);
	}

	public void addObject(String key, Object value) {
		putAttr(key, value);
	}

	public ModelAndView() {
	}

	public ModelAndView(String path) {
		view.setPath(path);
	}
}
