/**
 * 
 */
package com.jplus.core.core.abstracts;

import java.util.Properties;

/**
 * 1.通过配置路径加载所有配置项目
 */
public abstract class Environment {

	/**
	 * 根据入参获取到Prop对象，入参一般为配置文件路径。<br>
	 * 具体加载规则，请查看当前容器所选加载器
	 */
	public abstract Properties loadConfigFile(String... confPath);
	
	public abstract Properties getProp( );

	public abstract String getProp(FRAME key);

	public abstract String getProp(FRAME key, String def);

	public abstract String getProp(String key);

	public abstract String getSysEnv(String key);

	public abstract String getProp(String key, String def);

	public abstract String getSysEnv(String key, String def);

	// 系统设定
	public abstract boolean addFrameProp(FRAME key, String value);

	// 用户设定
	public abstract boolean addUserProp(String key, String value);

	public static enum FRAME {
		COMPONENT_SCAN("frame.component-scan"), 
		LAZY_INIT("frame.lazy.init"),
		FILE_PROP("frame.file.properties"),
		
		;
		String value;

		private FRAME(String value) {
			this.value = value;
		}
	}

}
