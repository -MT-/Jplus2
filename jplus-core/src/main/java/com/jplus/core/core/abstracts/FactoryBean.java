package com.jplus.core.core.abstracts;

import com.jplus.core.core.beans.BeanDefinition;
import com.jplus.core.core.beans.BeanWrapper;

public abstract class FactoryBean {

	/**
	 * 根据bean定义 构建 bean实例
	 */
	public abstract <T> BeanWrapper<T> getObject(BeanDefinition beanDefinition);

}
