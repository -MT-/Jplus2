package com.jplus.core.core.beans;

import com.jplus.core.anno.com.Order;
import com.jplus.core.utill.JUtil;

/**
 * 【核心】Bean索引Key，可根据Class 或 Name 定位Bean对象
 */
public class BeanKey implements Comparable<BeanKey> {

	private String name;
	private Class<?> beanClass;// 必须要
	// 顺序
	private int order = Order.DEF;

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public BeanKey(Class<?> beanClass) {
		this.beanClass = beanClass;
		this.name = JUtil.firstCharToLowerCase(beanClass.getSimpleName());
	}

	public BeanKey(String name, Class<?> beanClass) {
		this.beanClass = beanClass;
		this.name = name;
		if (JUtil.isEmpty(name))
			this.name = JUtil.firstCharToLowerCase(beanClass.getSimpleName());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Class<?> getBeanClass() {
		return beanClass;
	}

	public void setBeanClass(Class<?> beanClass) {
		this.beanClass = beanClass;
	}

	/**
	 * Hash分桶beanClass 区分,必填项
	 */
	@Override
	public int hashCode() {
		return beanClass.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BeanKey) {
			BeanKey bk = (BeanKey) obj;
			// 优先匹配Name
			String bkName = bk.getName();
			if (bkName != null && !bkName.equals("") && bkName.equals(this.name)) {
				return true;
			}

			Class<?> bkClass = bk.getBeanClass();
			if (bkClass != null && bkClass.equals(this.beanClass)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "BeanKey [name=" + name + ", beanClass=" + beanClass + ", order=" + order + "]";
	}

	@Override
	public int compareTo(BeanKey obj) {
		return this.order - obj.order;
	}

}
