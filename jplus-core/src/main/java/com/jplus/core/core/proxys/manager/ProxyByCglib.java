package com.jplus.core.core.proxys.manager;

import java.lang.reflect.Method;
import java.util.List;

import com.jplus.core.core.proxys.Proxy;
import com.jplus.core.core.proxys.ProxyChain;
import com.jplus.core.core.proxys.ProxyManager;
import com.jplus.core.core.proxys.ProxyMethod;
import com.jplus.core.core.proxys.ProxyMethod.ProxyType;

public class ProxyByCglib implements ProxyManager {

	/**
	 * 创建Cglib代理
	 * 
	 * @param targetClass
	 *            被代理的类
	 * @param proxyList
	 *            拦截代理类的拦截类
	 * @param originObj
	 *            被代理的类的实际对象[选填，如果在拦截类里面希望获取到实际类，可以使用]
	 * @return 返回代理对象
	 */
	public Object createProxy(final List<Proxy> proxyList, final Class<?>... targetClass) {
		return net.sf.cglib.proxy.Enhancer.create(targetClass[0], new net.sf.cglib.proxy.MethodInterceptor() {
			@Override
			public Object intercept(Object targetObject, Method targetMethod, Object[] methodParams, net.sf.cglib.proxy.MethodProxy methodProxy) throws Throwable {
				return new ProxyChain(targetClass[0], null, targetObject, targetMethod, methodParams, new ProxyMethod(methodProxy, ProxyType.Cglib), proxyList).doProxyChain();
			}
		});
	}
}
