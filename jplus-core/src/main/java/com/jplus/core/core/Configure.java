package com.jplus.core.core;

import com.jplus.core.core.abstracts.BeanDefinitions;
import com.jplus.core.core.abstracts.BeanFactory;
import com.jplus.core.core.abstracts.Environment;
import com.jplus.core.core.abstracts.FactoryBean;

/**
 * 系统组件自定义
 * 
 * @author yuanqy
 */
public interface Configure {

	public Environment getEnvironment();

	public BeanDefinitions getBeanDefinitions();

	public FactoryBean getFactoryBean();

	public BeanFactory getBeanFactory();

}
