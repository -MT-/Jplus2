package com.jplus.core.core;

import com.jplus.core.core.abstracts.BeanDefinitions;
import com.jplus.core.core.abstracts.BeanFactory;
import com.jplus.core.core.abstracts.Environment;
import com.jplus.core.core.abstracts.FactoryBean;
import com.jplus.core.core.abstracts.defaults.DefaultBeanDefinition;
import com.jplus.core.core.abstracts.defaults.DefaultBeanFactory;
import com.jplus.core.core.abstracts.defaults.DefaultEnvironment;
import com.jplus.core.core.abstracts.defaults.DefaultFactoryBean;
import com.jplus.core.core.proxys.ProxyManager;
import com.jplus.core.core.proxys.manager.ProxyByJavassist;

/**
 * 默认容器. 用户自定义容器可以继承ApplicationContext 重写接口方法。
 * 
 * @author Yuanqy
 */
public class AnnotationConfigApplicationContext extends ApplicationContext {

	public AnnotationConfigApplicationContext() {

	}

	public AnnotationConfigApplicationContext(Class<?>... configClasses) {
		register(configClasses);
		refresh();
	}

	public AnnotationConfigApplicationContext(String... basePackages) {
		scan(basePackages);
		refresh();
	}

	@Override
	public Environment getEnvironment() {
		return environment != null ? environment : DefaultEnvironment.getSingleton();
	}

	@Override
	public BeanDefinitions getBeanDefinitions() {
		return beanDefinition != null ? beanDefinition : new DefaultBeanDefinition();
	}

	@Override
	public FactoryBean getFactoryBean() {
		return factoryBean != null ? factoryBean : new DefaultFactoryBean(this);
	}

	@Override
	public BeanFactory getBeanFactory() {
		return beanFactory != null ? beanFactory : new DefaultBeanFactory(this);
	}

	@Override
	public ProxyManager getProxyManager() {
		return proxyManager != null ? proxyManager : new ProxyByJavassist();
	}

}
