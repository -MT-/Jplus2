package com.jplus.core.core.logger;

import java.net.URL;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.classic.spi.Configurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.spi.ContextAwareBase;

/**
 * LogbackConfigurator
 */
public class LogbackProvider extends ContextAwareBase implements Configurator {
    // 可见，logback加载的过程是
    // （1）使用logback.configurationFile环境变量的设置
    // （2）使用classpath中的logback.groovy
    // （3）使用classpath中的logback-test.xml
    // （4）使用classpath中的logback.xml
    // （5）查找com.qos.logback.classic.spi.Configurator接口实现类，调用实现类的configure方法设置
    // （6）使用BasicConfigurator类的configure方法设置
    @Override
    public void configure(LoggerContext loggerContext) {
        // 清除loggerContext已加载配置，重新加载
        loggerContext.reset();
        JoranConfigurator configurator = new JoranConfigurator();
        try {
            // 获取jar中默认配置文件路径
            URL url = Configurator.class.getClassLoader().getResource("log/logback/base.xml");
            // 设置loggerContext到JoranConfigurator
            configurator.setContext(loggerContext);
            // 加载默认配置
            configurator.doConfigure(url);
        } catch (JoranException e) {
            this.addError("Failed to instantiate [" + LoggerContext.class.getName() + "]", e);
        }
    }

}