package com.jplus.core.core.abstracts;

import java.util.List;
import java.util.Map;

import com.jplus.core.core.beans.BeanDefinition;
import com.jplus.core.core.beans.BeanKey;

/**
 * 2.通过包名路径加载bean定义
 * 
 * @author yuanqy
 */
public abstract class BeanDefinitions {

	/**
	 * 仅用于获取已有的BeanKey,并非生成新的
	 */
	public abstract BeanKey getBeanKey(String beanName, Class<?> beanClass);

	/**
	 * 批量创建bean定义，并返回所有
	 */
	public abstract Map<BeanKey, BeanDefinition> buildBeanDefinition(String packagePath);

	/**
	 * 创建单个bean定义，并返回当前
	 */
	public abstract BeanDefinition buildBeanDefinition(String name, Class<?> clazz);

	/**
	 * 删除bean定义
	 */
	public abstract void removeBeanDefinition(BeanKey key);

	/**
	 * 获取单个bean定义
	 */
	public abstract BeanDefinition getBeanDefinition(BeanKey key);

	/**
	 * 获取所有bean定义
	 */
	public abstract Map<BeanKey, BeanDefinition> getBeanDefinitions();

	/**
	 * 获取有序的Bean定义，按@Order 排序
	 */
	public abstract List<BeanDefinition> listSortBeanDefinitions();
}
