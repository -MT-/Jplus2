package com.jplus.core.core.proxys.manager;

import java.lang.reflect.Method;
import java.util.List;

import com.jplus.core.core.proxys.Proxy;
import com.jplus.core.core.proxys.ProxyChain;
import com.jplus.core.core.proxys.ProxyManager;
import com.jplus.core.core.proxys.ProxyMethod;
import com.jplus.core.core.proxys.ProxyMethod.ProxyType;

public class ProxyByJavassist implements ProxyManager {

	/**
	 * 创建JavaSsist代理<br>
	 * 
	 * @param targetClass
	 *            被代理的类
	 * @param proxyList
	 *            拦截代理类的拦截类
	 * @param originObj
	 *            被代理的类的实际对象 [选填，如果在拦截类里面希望获取到实际类，可以使用]
	 * @return 返回代理对象
	 */
	public Object createProxy(final List<Proxy> proxyList, final Class<?>... targetClass) {
		Object proxyObj = null;
		try {
			javassist.util.proxy.ProxyFactory f = new javassist.util.proxy.ProxyFactory();
			f.setSuperclass(targetClass[0]);
			f.setFilter(new javassist.util.proxy.MethodFilter() {// 方法过滤器
				public boolean isHandled(Method m) {
					// ignore finalize()
					return !m.getName().equals("finalize");// finalize是gc调用方法
				}
			});
			Class<?> cproxy = f.createClass();// 生成代理Class
			javassist.util.proxy.MethodHandler mi = new javassist.util.proxy.MethodHandler() {// 方法拦截器
				public Object invoke(Object proxyObject, Method targetMethod, Method proxyMethod, Object[] args) throws Throwable {
					return new ProxyChain(targetClass[0], null, proxyObject, targetMethod, args, new ProxyMethod(proxyMethod, ProxyType.JavaSsist), proxyList).doProxyChain();
				}
			};
			proxyObj = cproxy.newInstance();// 代理实例化
			((javassist.util.proxy.Proxy) proxyObj).setHandler(mi);// 添加拦截器
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return proxyObj;
	}

}
