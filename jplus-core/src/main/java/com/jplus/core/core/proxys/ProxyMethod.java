package com.jplus.core.core.proxys;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.MethodProxy;

public class ProxyMethod {

    public static enum ProxyType {
        Cglib, JavaSsist, JDK
    }

    private Object proxyMethod;
    private ProxyType type;



    public ProxyType getType() {
        return type;
    }

    /**
     * @param proxyMethod 代理方法，必填项【jdk就填原始方法】
     * @param type
     */
    public ProxyMethod(Object proxyMethod, ProxyType proxyType) {
        this.proxyMethod = proxyMethod;
        this.type = proxyType;
    }



    public Object invoke(Object proxyObject, Object... methodParams) throws Throwable {
        switch (type) {
            case Cglib:
                return ((MethodProxy) proxyMethod).invokeSuper(proxyObject, methodParams);
            case JDK:
            case JavaSsist:// jdk 和 ssist 一样
                return ((Method) proxyMethod).invoke(proxyObject, methodParams);
        }
        return null;
    }
}
