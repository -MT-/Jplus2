package com.jplus.core.core.events;

public interface ApplicationEventPublisher {

	void publishEvent(ApplicationEvent event);

}