package com.jplus.core.core;

/**
 * 宕机钩子
 * 
 * @author Yuanqy
 */
public class ShutdownHook extends Thread {

	private ApplicationContext context;

	public ShutdownHook(ApplicationContext context) {
		this.context = context;
	}

	public void run() {
		this.context.preDestory();
	}
}
