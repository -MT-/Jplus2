package com.jplus.core.core.beans;


/**
 * 【核心】Bean对象包装，包含真实实体对象 或 最终实体对象
 */
public class BeanWrapper<T> {

    private T bean;

    private T proxy;

    public T getBean() {
        return proxy != null ? proxy : bean;
    }

    public BeanWrapper(T bean, T proxy) {
        this.bean = bean;
        this.proxy = proxy;
    }

}
