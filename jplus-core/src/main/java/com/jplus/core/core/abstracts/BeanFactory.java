
package com.jplus.core.core.abstracts;

import java.util.Map;

import com.jplus.core.anno.com.Scope.EScope;
import com.jplus.core.core.ApplicationContext;
import com.jplus.core.core.beans.BeanDefinition;
import com.jplus.core.core.beans.BeanKey;
import com.jplus.core.core.beans.BeanWrapper;
import com.jplus.core.core.events.ApplicationEvent;
import com.jplus.core.core.events.ApplicationListener;
import com.jplus.core.fault.BeansException;

/**
 * Bean工厂接口
 * 
 * @author yuanqy
 */
public abstract class BeanFactory {

	protected Map<BeanKey, BeanWrapper<?>> beans;// 单例 实例池
	protected ThreadLocal<Map<BeanKey, BeanWrapper<?>>> thBeans;// 线程 实例池
	protected ApplicationContext context;
	// * =================================================

	public abstract Object getBean(String name) throws BeansException;

	public abstract <T> T getBean(String name, Class<T> requiredType) throws BeansException;

	public abstract <T> T getBean(Class<T> requiredType) throws BeansException;

	public abstract <T> T getBean(BeanKey beanKey);

	/**
	 * 实例化bean
	 */
	public abstract void buildBean();

	public abstract void buildBean(BeanDefinition bd);

	/**
	 * 添加已有对象实例
	 */
	public abstract boolean buildBean(BeanKey beanKey, Object bean, EScope scope);

	public abstract void removeBean(BeanDefinition bd);

	/**
	 * 获取事件Bean
	 */
	public abstract <T extends ApplicationListener<ApplicationEvent>> T getEventBean(ApplicationEvent event);

}
