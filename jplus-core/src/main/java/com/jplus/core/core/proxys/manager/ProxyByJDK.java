package com.jplus.core.core.proxys.manager;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;

import com.jplus.core.core.proxys.Proxy;
import com.jplus.core.core.proxys.ProxyChain;
import com.jplus.core.core.proxys.ProxyManager;
import com.jplus.core.core.proxys.ProxyMethod;
import com.jplus.core.core.proxys.ProxyMethod.ProxyType;

public class ProxyByJDK implements ProxyManager {
	/**
	 * 创建JDK代理<br>
	 * 
	 * @param proxyList
	 *            拦截代理类的拦截类
	 * @param originObj
	 *            被代理的类的实际对象
	 * @return <strong>返回对象Object 必须用接口对象接收,否则转换失败</strong>
	 */
	public Object createProxy(final List<Proxy> proxyList, final Class<?>... targetInterfaces) {
		return java.lang.reflect.Proxy.newProxyInstance(ProxyManager.class.getClassLoader(), targetInterfaces, new InvocationHandler() {
			@Override
			public Object invoke(Object proxyObject, Method targetMethod, Object[] args) throws Throwable {
				return new ProxyChain(null, targetInterfaces, proxyObject, targetMethod, args, new ProxyMethod(targetMethod, ProxyType.JDK), proxyList).doProxyChain();
			}
		});
	}

}
