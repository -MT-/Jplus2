package com.jplus.core.core.proxys;

import java.util.List;

/**
 * 代理管理器<br>
 * 接口代理使用JDK,对象代理使用Cglib\Ssist
 */
public interface ProxyManager {
	public Object createProxy(final List<Proxy> proxyList, final Class<?> ... targetClass);

}
