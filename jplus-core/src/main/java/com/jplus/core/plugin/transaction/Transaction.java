package com.jplus.core.plugin.transaction;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 定义需要事务控制的方法
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Transaction {
	/**
	 * 事务隔离级别：<br>
	 * TRANSACTION_NONE=0<br>
	 * TRANSACTION_READ_UNCOMMITTED=1<br>
	 * TRANSACTION_READ_COMMITTED=2<br>
	 * TRANSACTION_REPEATABLE_READ=4<br>
	 * TRANSACTION_SERIALIZABLE=8<br>
	 */
	int level() default 4;
}
