package com.jplus.core.plugin.scheduling;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 每个Scheduled 修饰的方法，都有一个独立线程，也可以选择使用异步线程池，使用@Async 线程池代理即可
 * 
 * @author Yuanqy
 *
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Scheduled {

	/**
	 * @see com.jplus.core.plugin.scheduling.cron.CronKit
	 * @return
	 */
	String cron() default "";

	/**
	 * Execute the annotated method with a fixed period in milliseconds between
	 * the end of the last invocation and the start of the next.
	 * 
	 * @return the delay in milliseconds
	 */
	long fixedDelay() default -1;

	/**
	 * Execute the annotated method with a fixed period in milliseconds between
	 * invocations.
	 * 
	 * 令牌桶限流算法
	 * 
	 * @return the period in milliseconds
	 */
	long fixedRate() default -1;

	/**
	 * Number of milliseconds to delay before the first execution of a
	 * 
	 * @return the initial delay in milliseconds
	 */
	long initialDelay() default -1;

}