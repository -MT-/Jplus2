package com.jplus.core.plugin;

import com.jplus.core.core.ApplicationContext;

public interface ImportSelector {

	String[] selectImports(ApplicationContext context );

}