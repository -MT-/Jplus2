package com.jplus.core.plugin.aspect;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 和 MethodInterceptor 结合使用
 * 
 * @author Yuanqy
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Aspect {
	/**
	 * 正则表达式规则：<返回类型模式>空格<包名.类名.方法名模式>(<参数模式>)
	 * <br>举例：
	 * <pre>
	 *  &#64;Aspect("String com.jplus.demo.service.ServiceClass.methodName(String,String)")
	 *  &#64;Aspect("* com.jplus.demo.service.*.*(..)")
	 *  &#64;Aspect("* com.jplus.*.service..*(*,String)")
	 *  &#64;Aspect("* com.jplus..service..*(..)")
	 * </pre>
	 * 
	 * <b>注意：返回类型和参数类型，使用不加包名的名称</b>
	 * 
	 */
	String[] value() default "";

	/**
	 * 注解模式，拦截有某些注解的类
	 * 
	 */
	Class<? extends Annotation>[] anno() default {};

}