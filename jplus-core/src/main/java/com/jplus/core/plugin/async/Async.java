package com.jplus.core.plugin.async;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 创建对象的线程池代理
 * 
 * @author Yuanqy
 *
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Async {

	/**
	 * 制定 实现Executor 的bean名称
	 * 
	 * @link java.util.concurrent.Executor Executor
	 */
	String value() default "";


	

}