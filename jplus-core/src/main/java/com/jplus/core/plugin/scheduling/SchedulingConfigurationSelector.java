package com.jplus.core.plugin.scheduling;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.jplus.core.core.ApplicationContext;
import com.jplus.core.core.beans.BeanDefinition;
import com.jplus.core.plugin.ImportSelector;

public class SchedulingConfigurationSelector implements ImportSelector {

	private String DEF_SCHEDULED = "com.jplus.core.plugin.scheduling.SchedulingConfiguration";
	private AtomicInteger i = new AtomicInteger(0);
	private static List<ScheduTask> listScheduTask = new ArrayList<>();

	public static List<ScheduTask> getListScheduTask() {
		return listScheduTask;
	}

	@Override
	public String[] selectImports(ApplicationContext context) {
		List<BeanDefinition> list = context.getBeanDefinitions().listSortBeanDefinitions();
		for (BeanDefinition bd : list) {
			Method[] methods = bd.getBeanClass().getMethods();
			for (Method method : methods) {
				Scheduled scheduled = method.getAnnotation(Scheduled.class);
				if (scheduled != null) {// 当方法上有@scheduled
					listScheduTask.add(new ScheduTask(context, bd.getBeanKey(), scheduled, method, "Task-" + i.addAndGet(1)));
				}
			}
		}
		return new String[] { DEF_SCHEDULED };
	}

}
