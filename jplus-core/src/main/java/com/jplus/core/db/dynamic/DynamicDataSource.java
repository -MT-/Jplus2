package com.jplus.core.db.dynamic;

/**
 * 动态数据源配置<br/>
 * DynamicDataSource可以配置N个数据源，在数据库操作中可以做到动态切换<br/>
 * 但在事物中，无法切换[切换无效]。<br/>
 * 
 * @author Yuanqy
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

	@Override
	public String determineCurrentLookupKey() {
		return DataSourceHolder.getDbType();
	}

}
