
/**
 * Aes十进制加密工具类，密钥请见 接口文档，偏移量和密钥相同
 * 改动点：
 * 1，修改了三个盒子模型。
 * 2，将异或算法修改为十进制模十算法
 * 
 * @author Yuanqy 
 * @Date 2019/07/05
 * @company http://www.lianyuplus.com/
*/
#include <stdio.h>
#include <string.h>

enum AESMode_t
{
    MODE_OFB = 1,
    MODE_CFB, // 门锁设备使用CFB加密
    // MODE_CBC,
    // MODE_ECB
};
typedef enum AESMode_t AESMode_t;
//==========================================
AESMode_t m_mode;        //当前加密模式
unsigned char m_key[16]; //key 密钥 len=Nx4
unsigned char m_iv[16];  //iv偏移量 len=Nx4

unsigned char w[11][4][4];

// 修改S盒模型，IV盒模型，行列偏移模型
// unsigned char Sbox[10] = {6, 8, 4, 9, 7, 2, 1, 0, 5, 3};
unsigned char Sbox[256] =
    {
        /*  0    1    2    3    4    5    6    7    8    9    a    b    c    d    e    f */
        0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76, /*0*/
        0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, /*1*/
        0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15, /*2*/
        0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, /*3*/
        0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, /*4*/
        0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, /*5*/
        0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, /*6*/
        0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, /*7*/
        0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73, /*8*/
        0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb, /*9*/
        0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, /*a*/
        0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08, /*b*/
        0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, /*c*/
        0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, /*d*/
        0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, /*e*/
        0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16  /*f*/
};
unsigned char rc[] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36};

//=========================================================
void KeyExpansion(unsigned char *key, unsigned char w[][4][4]);
unsigned char FFmul(unsigned char a, unsigned char b);
//=========================================================
void SetKey(unsigned char *key);
void Cipher(unsigned char *input, unsigned char *output);

void SubBytes(unsigned char state[][4]);
void ShiftRows(unsigned char state[][4]);
void MixColumns(unsigned char state[][4]);
void AddRoundKey(unsigned char state[][4], unsigned char k[][4]);
//=========================================================
void set_mode(AESMode_t _mode);
void set_key(unsigned char *key);
void set_iv(unsigned char *iv);
int Encrypt(unsigned char *input, int length, unsigned char *output);
int Decrypt(unsigned char *input, int length, unsigned char *output);
//=========================================================
void print(unsigned char *state, int len);
//=========================================================

// 设置密钥
void SetKey(unsigned char *key)
{
    KeyExpansion(key, w);
    for (size_t i = 0; i < 11; i++)
    {
        for (size_t c = 0; c < 4; c++)
        {
            for (size_t r = 0; r < 4; r++)
            {
                printf(" %02X ", w[i][r][c]);
            }
            printf("\n");
        }
        printf("\n");
    }
}

void Cipher(unsigned char *input, unsigned char *output)
{
    unsigned char state[4][4];
    int i, r, c;
    for (r = 0; r < 4; r++)
        for (c = 0; c < 4; c++)
            state[r][c] = input[c * 4 + r];

    AddRoundKey(state, w[0]); //轮密钥加
    for (i = 1; i <= 10; i++)
    {
        SubBytes(state);  //字节替换
        ShiftRows(state); //行位移
        if (i != 10)
            MixColumns(state);    //列混合
        AddRoundKey(state, w[i]); //轮密钥加
    }

    for (r = 0; r < 4; r++)
        for (c = 0; c < 4; c++)
            output[c * 4 + r] = state[r][c];
    printf("=xxxxxxx\n");
    print(output, 16);
}

void KeyExpansion(unsigned char *key, unsigned char w[][4][4])
{
    int i, j, r, c;
    for (r = 0; r < 4; r++)
        for (c = 0; c < 4; c++)
            w[0][r][c] = key[r + c * 4];

    for (i = 1; i <= 10; i++)
    {
        for (j = 0; j < 4; j++)
        {
            unsigned char t[4];
            for (r = 0; r < 4; r++)
                t[r] = j ? w[i][r][j - 1] : w[i - 1][r][3];

            if (j == 0)
            {
                int temp = t[0];
                for (r = 0; r < 3; r++)
                {
                    // printf("%d ", t[(r + 1) % 4]);
                    t[r] = Sbox[t[(r + 1) % 4]];
                }

                t[3] = Sbox[temp];
                t[0] = (t[0] ^ rc[i - 1]);
            }
            for (r = 0; r < 4; r++)
                w[i][r][j] = (w[i - 1][r][j] ^ t[r]);
        }
    }
}

unsigned char FFmul(unsigned char a, unsigned char b)
{
    unsigned char bw[4];
    unsigned char res = 0;
    int i;
    bw[0] = b;
    for (i = 1; i < 4; i++)
    {
        bw[i] = bw[i - 1] << 1;
        if (bw[i - 1] & 0x80)
            bw[i] ^= 0x1b;
    }
    for (i = 0; i < 4; i++)
    {
        if ((a >> i) & 0x01)
            res ^= bw[i];
    }
    printf("FFmul[%d,%d]:%d \n", a, b, res);
    return res;
}

void SubBytes(unsigned char state[][4])
{
    int r, c;
    for (r = 0; r < 4; r++)
    {
        for (c = 0; c < 4; c++)
            state[r][c] = Sbox[state[r][c]];
    }
}

void ShiftRows(unsigned char state[][4])
{
    unsigned char t[4];
    int r, c;
    for (r = 1; r < 4; r++)
    {
        for (c = 0; c < 4; c++)
            t[c] = state[r][(c + r) % 4];
        for (c = 0; c < 4; c++)
            state[r][c] = t[c];
    }
}

void MixColumns(unsigned char state[][4])
{
    unsigned char t[4];
    int r, c;
    for (c = 0; c < 4; c++)
    {
        for (r = 0; r < 4; r++)
            t[r] = state[r][c];
        for (r = 0; r < 4; r++)
            state[r][c] = (FFmul(0x02, t[r]) ^ FFmul(0x03, t[(r + 1) % 4]) ^ FFmul(0x01, t[(r + 2) % 4]) ^ FFmul(0x01, t[(r + 3) % 4])) % 255;
    }
}

void AddRoundKey(unsigned char state[][4], unsigned char k[][4])
{
    int r, c;
    for (c = 0; c < 4; c++)
        for (r = 0; r < 4; r++)
        {
            printf("%d^^^%d\n", state[r][c], k[r][c]);
            state[r][c] = (state[r][c] ^ k[r][c]) % 10;
            printf("AddRoundKey:%d\n", state[r][c]);
        }
}

/**************************************************************/
void set_mode(AESMode_t _mode)
{
    m_mode = _mode;
}

void set_key(unsigned char *_key)
{
    memcpy(m_key, _key, 16);
    SetKey(m_key);
}
void set_iv(unsigned char *_iv)
{
    memcpy(m_iv, _iv, 16);
}

int Encrypt(unsigned char *_in, int _length, unsigned char *_out)
{
    for (int x = 0; x < _length / 2; x++)
    {
        if (x % 2 == 0)
        {
            _in[x] ^= _in[_length - 1 - x];
            _in[_length - 1 - x] ^= _in[x];
            _in[x] ^= _in[_length - 1 - x];
        }
    }
    print(_in, _length);
    int first_round = 1;
    int rounds = 0;
    int start = 0;
    int end = 0;
    unsigned char input[16] = {0};
    unsigned char output[16] = {0};
    unsigned char ciphertext[16] = {0};
    unsigned char cipherout[256] = {0};
    unsigned char plaintext[16] = {0};
    int co_index = 0;
    // 1. get rounds
    if (_length % 16 == 0)
        rounds = _length / 16;
    else
        rounds = _length / 16 + 1;
    // 2. for all rounds
    for (int j = 0; j < rounds; ++j)
    {
        start = j * 16;
        end = j * 16 + 16;
        if (end > _length)
            end = _length; // end of input
        // 3. copyt input to m_plaintext
        memset(plaintext, 0, 16);
        memcpy(plaintext, _in + start, end - start);
        // 4. handle all modes
        if (m_mode == MODE_CFB)
        {
            if (first_round == 1)
            {
                Cipher(m_iv, output);
                first_round = 0;
            }
            else
            {
                Cipher(input, output);
            }
            // print(output, 16);
            for (int i = 0; i < 16; ++i)
            {
                if ((end - start) - 1 < i)
                {
                    ciphertext[i] = 0 ^ output[i];
                }
                else
                {
                    printf("%d---%d\n", output[i], ciphertext[i]);
                    int b = (output[i] * (i > 0 ? ciphertext[i - 1] : 9)) % 10;
                    ciphertext[i] = (plaintext[i] + 2 + b) % 10 + 0x30;
                    printf("p:%X - x:%X \n", ciphertext[i], b);
                }
            }
            for (int k = 0; k < end - start; ++k)
            {
                cipherout[co_index++] = ciphertext[k];
            }
            memset(input, 0, 16);
            memcpy(input, ciphertext, 16);
        }
    }
    memcpy(_out, cipherout, co_index);
    _out[co_index] = '\0'; //在字符串最后添加'\0'字符，c语言字符串以'\0'结束。
    return co_index;
}

int Decrypt(unsigned char *_in, int _length, unsigned char *_out)
{
    // TODO :
    int first_round = 1;
    int rounds = 0;
    unsigned char ciphertext[16] = {0};
    unsigned char input[16] = {0};
    unsigned char output[16] = {0};
    unsigned char plaintext[16] = {0};
    unsigned char plainout[256] = {0};
    int po_index = 0;
    if (_length % 16 == 0)
    {
        rounds = _length / 16;
    }
    else
    {
        rounds = _length / 16 + 1;
    }

    int start = 0;
    int end = 0;

    for (int j = 0; j < rounds; j++)
    {
        start = j * 16;
        end = start + 16;
        if (end > _length)
        {
            end = _length;
        }
        memset(ciphertext, 0, 16);
        memcpy(ciphertext, _in + start, end - start);
        if (m_mode == MODE_CFB)
        {
            if (first_round == 1)
            {
                Cipher(m_iv, output);
                first_round = 0;
            }
            else
            {
                Cipher(input, output);
            }
            for (int i = 0; i < 16; i++)
            {
                //##############################
                int c = (ciphertext[i] - 0x30);
                // printf("===c:%d\n", c);
                int b = (output[i] * (i > 0 ? ciphertext[i - 1] : 9)) % 10;
                plaintext[i] = (c >= b ? c - b : c - b + 10) + 0x30;
                // printf("p:%X - x:%X \n", plaintext[i], b);
            }
            for (int k = 0; k < end - start; ++k)
            {
                plainout[po_index++] = plaintext[k];
            }
            memcpy(input, ciphertext, 16);
        }
    }
    memcpy(_out, plainout, po_index);
    for (int x = 0; x < _length / 2; x++)
    {
        if (x % 2 == 0)
        {
            _out[x] ^= _out[_length - 1 - x];
            _out[_length - 1 - x] ^= _out[x];
            _out[x] ^= _out[_length - 1 - x];
        }
    }
    _out[po_index] = '\0'; //在字符串最后添加'\0'字符，c语言字符串以'\0'结束。
    return po_index;
}

void print(unsigned char *state, int len)
{
    int i;
    for (i = 0; i < len; i++)
    {
        if (state[i] < 16)
            printf("0%X  ", (int)(state[i] & 0xff));
        else
            printf("%X  ", (int)(state[i] & 0xff));
    }
    printf("\n");
}

/**
 * ========================================================
 * ========================================================
 * ========================================================
*/
int main(int argc, char const *argv[])
{
    unsigned char input[] = "123123455";      //数据内容，必须为数字
    unsigned char ivs[] = "UCG6F78XpLoFcUwk"; //iv 和 key 可以相同，并且支持数字字母字符,长度必须 16
    unsigned char key[] = "UCG6F78XpLoFcUwk";
    unsigned char output[100]; //加密密文
    unsigned char dinput[100]; //解密明文
    //	unsigned char
    set_key(key);
    set_mode(MODE_CFB); //CFB   支持
    set_iv(ivs);
    int olen = strlen(input);

    printf("\nEncrypt----------\n");
    int len = Encrypt(input, olen, output);
    printf("加密HEX[%d]:\t", len);
    print(output, len);

    printf("加密密文：\t%s", output);
    printf("\nDecrypt----------\n");
    // len = Decrypt("4723740802", len, dinput);
    len = Decrypt(output, len, dinput);
    printf("解密HEX[%d]:\t", len);
    printf("解密明文：\t%s", dinput);
    return 0;
}
