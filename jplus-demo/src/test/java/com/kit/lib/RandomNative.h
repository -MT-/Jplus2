#ifndef RANDOMNATIVE_H
#define RANDOMNATIVE_H
//需要硬件端提供的接口函数

//== 针对存储数组 ============================================================
void putFlashData(char *name, char *data); //存储一个256byte数组到flash中（数组名，数组）；
int getFlashData(char *name, int index);   // 通过下标取flash的值 (数组名，下标ID，返回当前val）；

//== 针对用户密码 ============================================================
int existPwd(char *pwd); // 判断密码是否存在，存在返回1，不存在返回0
void clearPwd();         // 清空所有存储的密码
void delPwd(char *pwd);  // 删除单个密码
void putPwd(char *pwd);  // 存储校验成功的所有密码，要去重

//== 北京时间GMT+8 操作接口 ===================================================
// 获取 当天所在月份的值（入参：unix时间戳）
int getDayOfMounth(time_t unixTime);
// 获取 当前小时 所在天的值（入参：unix时间戳）
int getHourOfDay(time_t unixTime);

#endif