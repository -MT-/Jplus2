﻿注意：密码开门中，所有时间比对。使用的是北京时间，而非UTC时间。

代码说明：
RandomNative.h		//硬件端需实现接口文件
RandomCipherHelper.c	//密码校验类
RandomSequence.c		//密码数组生成类

RandomCipherHelper.java	//java 密码生成& 校验类
RandomSequence.java	//java 密码数组生成类



必要条件【见接口文档：初始化密码库】：
initHour		：设置基础时间，密码开始时间的偏移量基于此
seed1		：随机种子1，用于生成高位偏移量
seed2		：随机种子2，用于生成低位偏移量

需要硬件端实现(见RandomNative.h)：


流程：
1，设备初始化时，接收初始化密码位命令。
2，通过参数seed1,和seed2,生成两个数组，并存储到flash中，提供按下标访问。
3，当用于在门锁上录入密码后(见RandomCipherHelper.c)，使用Aes10解密，然后执行校验密码doParse()。
4，校验通过后，需要将当前密码录入到用户密码存储空间中。并提供查找操作。
