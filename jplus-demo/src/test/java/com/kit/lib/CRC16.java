package com.kit.lib;

import com.jplus.core.utill.JUtil;

/**
 * CRC-16/DNP x16+x13+x12+x11+x10+x8+x6+x5+x2+1 <br>
 * 是低位在前，高位在后。
 * 
 * @see http://www.ip33.com/crc.html
 * @author Yuanqy
 * @Date 2019年7月8日
 * @company http://www.lianyuplus.com/
 */
public class CRC16 {

	public static void main(String[] args) {

		byte data[] = JUtil.hex2Bytes("128CB0D85D9CBED85D1DA0");
		byte[] crc = buildCRC16(data, data.length);
		System.out.println("校验位:" + byte2hex(crc));
	}

	/******************************************************************************
	 * 生成crc16 <br>
	 * Name: CRC-16/DNP x16+x13+x12+x11+x10+x8+x6+x5+x2+1 <br>
	 * Poly: 0x3D65 <br>
	 * Init:0x0000 <br>
	 * Refin: True <br>
	 * Refout: True <br>
	 * Xorout: 0xFFFF <br>
	 * Use: M-Bus,ect.<br>
	 *****************************************************************************/
	static byte[] buildCRC16(byte[] buf, int len) {
		// ==【改动部分：将byte转为16进制字符后处理】==========
		String hex = byte2hex(buf);
		// System.out.println("Ascii :" + hex);
		buf = hex.getBytes();
		len *= 2;
		// =================================================
		Integer crc = 0x0000;// Initial value
		int temp, i = 0;
		for (int x = 0; x < len; x++) {
			temp = buf[x] < 0 ? buf[x] & 0xFF : buf[x];// byte [-128~127]
			crc ^= temp; // crc ^= *data; data++;
			for (i = 0; i < 8; ++i) {
				if ((crc & 1) > 0)
					crc = (crc >> 1) ^ 0xA6BC; // 0xA6BC = reverse 0x3D65
				else
					crc = (crc >> 1);
			}
		}
		return getBytes(crc ^ 0xFFFF); // crc^Xorout
	}

	// ==============================================
	private static byte[] getBytes(int value) {
		byte[] var1 = new byte[] { (byte) value, (byte) (value >> 8) };// 低位在前，高位在后
		return var1;
	}

	private static String byte2hex(byte[] b) {
		String hs = "";
		String stmp = "";
		for (int n = 0; n < b.length; n++) {
			stmp = (java.lang.Integer.toHexString(b[n] & 0XFF));
			if (stmp.length() == 1)
				hs = hs + "0" + stmp;
			else
				hs = hs + stmp;
		}
		return hs.toUpperCase();
	}
}
