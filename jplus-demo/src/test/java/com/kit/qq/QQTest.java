package com.kit.qq;

import com.kit.qq.pctim.QQUser;
import com.kit.qq.pctim.robot.QQRobot;
import com.kit.qq.pctim.robot.RobotApi;
import com.kit.qq.pctim.sdk.Plugin;
import com.kit.qq.pctim.sdk.QQMessage;
import com.kit.qq.pctim.socket.LoginManager;
import com.kit.qq.pctim.socket.MessageResult.EType;
import com.kit.qq.pctim.socket.MessageService;
import com.kit.qq.pctim.util.Util;

public class QQTest {

	public static void main(String[] args) throws InterruptedException {
		String qqNum = "2697014126";
		String qqPwd = "yqy563930552";
		QQUser user = new QQUser(Long.valueOf(qqNum), Util.MD5(qqPwd));
		LoginManager login = new LoginManager(user);
		if (login.login().type == EType.SUCCESS) {
			QQRobot robot = new QQRobot();
			robot.setPlugins(new Plugin(new RobotApi(login.socket, user)) {
				@Override
				public void onMessageHandler(QQMessage message) {
					System.out.println(message);
					// MessageFactory factory = new MessageFactory();
					//// factory.Friend_uin = message.Sender_Uin;
					// api.SendFriendMessage(factory);
				}
			});
			MessageService service = new MessageService(user, login.socket, robot);
			service.start();
		}
		Thread.sleep(1000 * 60 * 60);
		System.out.println();

	}

}
