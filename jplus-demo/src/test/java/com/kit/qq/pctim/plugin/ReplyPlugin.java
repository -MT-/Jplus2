package com.kit.qq.pctim.plugin;

import com.kit.qq.pctim.sdk.API;
import com.kit.qq.pctim.sdk.MessageFactory;
import com.kit.qq.pctim.sdk.Plugin;
import com.kit.qq.pctim.sdk.QQMessage;

public class ReplyPlugin extends Plugin {

	public ReplyPlugin(API api) {
		super(api);
	}

	@Override
	public void onMessageHandler(QQMessage message) {
		String msg = message.Message;
		api.SendFriendMessage(new MessageFactory());
	}

}
