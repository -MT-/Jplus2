package com.kit.qq.pctim.sdk;

import java.awt.image.BufferedImage;

public class MessageFactory {
	public int message_type;
	public long Group_uin;
	public String Message;
	public long Friend_uin;
	public BufferedImage image;
	public byte[] imagedata;
	public String key30;
	public String key48;

	public MessageFactory() {

	}
}
