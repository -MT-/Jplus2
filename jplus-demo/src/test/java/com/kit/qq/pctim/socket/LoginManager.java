package com.kit.qq.pctim.socket;

import java.util.Date;

import com.kit.qq.pctim.QQGlobal;
import com.kit.qq.pctim.QQUser;
import com.kit.qq.pctim.pkg.ParseRecivePackage;
import com.kit.qq.pctim.pkg.SendPackageFactory;
import com.kit.qq.pctim.socket.MessageResult.EType;
import com.kit.qq.pctim.util.Util;

public class LoginManager {
	String verifyCode;
	private byte[] data = null;
	public Udpsocket socket = null;
	private QQUser _user;

	public LoginManager(QQUser user) {
		this._user = user;
		user.TXProtocol.DwServerIP = "sz.tencent.com";
		socket = new Udpsocket(user);
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

	public void relogin() {
		data = SendPackageFactory.get0825(_user);
		socket.sendMessage(data);
	}

	public MessageResult login() {
		data = SendPackageFactory.get0825(_user);
		socket.sendMessage(data);
		byte[] result = socket.receiveMessage();
		// System.out.println(Util.byte2HexString(result));
		ParseRecivePackage parsereceive = new ParseRecivePackage(result, _user.QQPacket0825Key, _user);
		parsereceive.decrypt_body();
		parsereceive.parse_tlv();
		while (parsereceive.Header[0] == -2) {
			System.out.println("重定向到:" + _user.TXProtocol.DwRedirectIP);
			_user.TXProtocol.WRedirectCount += 1;
			_user.IsLoginRedirect = true;
			socket = new Udpsocket(_user);
			data = SendPackageFactory.get0825(_user);
			socket.sendMessage(data);
			result = socket.receiveMessage();
			parsereceive = new ParseRecivePackage(result, _user.QQPacket0825Key, _user);
			parsereceive.decrypt_body();
			parsereceive.parse_tlv();
		}

		System.out.println("服务器连接成功,开始登陆");
		data = SendPackageFactory.get0836(_user, false);
		socket.sendMessage(data);
		result = socket.receiveMessage();
		parsereceive = new ParseRecivePackage(result, _user.TXProtocol.BufDhShareKey, _user);
		parsereceive.parse0836();

		switch (parsereceive.Header[0]) {
		case 52:
			return new MessageResult(EType.ACCOUNTERROR);
		case -5:
			return new MessageResult(EType.NEEDVERIFY);
		default:
			break;
		}

		if (parsereceive.Header[0] == 0) {
			_user.islogined = true;
			_user.logintime = new Date().getTime();
			data = SendPackageFactory.get0828(_user);
			socket.sendMessage(data);
			result = socket.receiveMessage();
			parsereceive = new ParseRecivePackage(result, _user.TXProtocol.BufTgtGtKey, _user);
			parsereceive.decrypt_body();
			parsereceive.parse_tlv();
			data = SendPackageFactory.get00ec(_user, QQGlobal.Online);
			socket.sendMessage(data);
			result = socket.receiveMessage();
			parsereceive = new ParseRecivePackage(result, _user.TXProtocol.SessionKey, _user);
			parsereceive.decrypt_body();
			data = SendPackageFactory.get001d(_user);
			socket.sendMessage(data);
			result = socket.receiveMessage();
			parsereceive = new ParseRecivePackage(result, _user.TXProtocol.SessionKey, _user);
			parsereceive.parse001d();
			Util.getquncookie(_user);
			System.out.println("成功获取用户信息: Nick: " + _user.NickName + " Age: " + _user.Age + " Sex: " + _user.Gender
					+ " bkn:" + _user.bkn + " Cookie:" + _user.quncookie);
			return new MessageResult(EType.SUCCESS);
		} else {
			return new MessageResult(EType.OTHERERROR);
		}

	}
}
