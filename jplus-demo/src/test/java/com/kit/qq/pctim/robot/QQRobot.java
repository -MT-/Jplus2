package com.kit.qq.pctim.robot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.kit.qq.pctim.sdk.Plugin;
import com.kit.qq.pctim.sdk.QQMessage;

public class QQRobot {

	List<Plugin> plugins = new ArrayList<Plugin>();

	public void setPlugins(Plugin... plugins) {
		this.plugins.addAll(Arrays.asList(plugins));
	}

	public void call(final QQMessage qqmessage) {
		for (final Plugin plugin : this.plugins) {
			new Thread() {
				public void run() {
					plugin.onMessageHandler(qqmessage);
				}
			}.start();
		}
	}

}
