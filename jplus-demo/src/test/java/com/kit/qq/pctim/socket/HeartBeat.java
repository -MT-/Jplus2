package com.kit.qq.pctim.socket;

import java.util.Date;

import com.kit.qq.pctim.QQUser;
import com.kit.qq.pctim.pkg.SendPackageFactory;

public class HeartBeat {
	private Thread thread;
	private long time_miles = 10000;

	public HeartBeat(final QQUser _user, final Udpsocket _socket) {
		this.thread = new Thread() {
			public void run() {
				while (true) {
					try {
						byte[] data = SendPackageFactory.get0058(_user);
						time_miles = new Date().getTime();
						_socket.sendMessage(data);
						Thread.sleep(time_miles);
					} catch (InterruptedException e) {
						System.out.println(e.getMessage());
					}
				}

			}
		};
	}

	public void start() {
		this.thread.setDaemon(true);
		this.thread.start();

	}
}
