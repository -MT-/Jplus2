package com.kit.qq.pctim.robot;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.kit.qq.pctim.QQUser;
import com.kit.qq.pctim.message.SendMessage;
import com.kit.qq.pctim.sdk.API;
import com.kit.qq.pctim.sdk.MessageFactory;
import com.kit.qq.pctim.socket.Udpsocket;

public class RobotApi implements API {
	private Udpsocket socket = null;
	private QQUser user = null;

	public RobotApi(Udpsocket _socket, QQUser _user) {
		this.user = _user;
		this.socket = _socket;
	}

	@Override
	public void SendGroupMessage(MessageFactory factory) {
		SendMessage.SendGroupMessage(this.user, this.socket, factory);
	}

	@Override
	public void SendFriendMessage(MessageFactory factory) {
		SendMessage.SendFriendMessage(this.user, this.socket, factory);
	}

	@Override
	public void GroupMemberShutUp(long gc, long uin, int time) {
		try {
			String urls = "http://qinfo.clt.qq.com/cgi-bin/qun_info/set_group_shutup";
			URL lll = new URL(urls);
			HttpURLConnection connection = (HttpURLConnection) lll.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Cookie", user.quncookie);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			String body = "";
			if (uin == 0) {
				if (time == 0) {
					body = "gc=" + gc + "&all_shutup=0&bkn=" + user.bkn + "&src=qinfo_v2";
				} else {
					body = "gc=" + gc + "&all_shutup=1&bkn=" + user.bkn + "&src=qinfo_v2";
				}
			} else {
				JSONArray data = new JSONArray();
				data.add(new JSONObject().fluentPut("uin", uin).fluentPut("t", time));
				body = "gc=" + gc + "&shutup_list=" + data.toString() + "&bkn=" + user.bkn + "&src=qinfo_v2";
			}
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
			writer.write(body);
			writer.close();
			connection.getResponseCode();
			connection.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
