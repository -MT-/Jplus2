package com.kit.qq.pctim.sdk;

public abstract class Plugin {

	protected API api;

	public Plugin(API api) {
		this.api = api;
	}

	public abstract void onMessageHandler(QQMessage message);
}
