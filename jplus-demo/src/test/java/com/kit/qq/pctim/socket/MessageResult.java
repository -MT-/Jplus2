package com.kit.qq.pctim.socket;

public class MessageResult {

	public enum EType {
		ACCOUNTERROR, // 密码错误
		NEEDVERIFY, // 需要验证码
		VERIFYERROR, // 验证码错误
		SUCCESS, // 登录成功
		OTHERERROR;
	}

	public EType type;

	public MessageResult(EType type) {
		this.type = type;
	}
}
