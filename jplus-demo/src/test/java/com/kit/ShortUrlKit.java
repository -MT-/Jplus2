package com.kit;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.jplus.core.utill.HttpClientUtil;

/**
 * 新浪短地址
 * 
 * @author Yuanqy
 *
 */
public class ShortUrlKit {

	public static void main(String[] args) {
		System.out.println(getUrl("http://k.lianyuplus.com/FAQs/ArticleID/189"));
	}

	private static HttpClientUtil http = new HttpClientUtil();

	public static Surl getUrl(String longUrl) {
		Map<String, Object> param = new HashMap<>();
		param.put("url_long", longUrl);
		String res = http.doGet("http://clicli.ink/s/build", param);
		System.out.println(res);
		return JSON.parseObject(res).getObject("data", Surl.class);
	}

	public static class Surl {
		private String url_short;
		private String url_long;

		public String getUrl_short() {
			return url_short;
		}

		public void setUrl_short(String url_short) {
			this.url_short = url_short;
		}

		public String getUrl_long() {
			return url_long;
		}

		public void setUrl_long(String url_long) {
			this.url_long = url_long;
		}

		@Override
		public String toString() {
			return "Surl [url_short=" + url_short + ", url_long=" + url_long + "]";
		}

	}
}
