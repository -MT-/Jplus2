
package com.kit;

import java.util.Arrays;

public class RandomSequence {

	public static void main(String[] args) {
		// int seed = 1797786002;// 种子
		int seed = 100000019;// 种子
		RandomSequence r = new RandomSequence(seed);
		// int count = 128;
		int count = 87616;
		// int count = 128;
		Integer[] seq = new Integer[64];
		for (int i = 1, j = count; i < count; i++, j--) {
			if (i >= j)
				break;
			int x = i % 32;
			seq[2 * x] = i;
			seq[2 * x + 1] = j;
			if (x == 0 && i > 0) {
				r.getSequence(seq);
				System.out.println(seq.length + ":" + Arrays.asList(seq));
			}
		}
	}

	private long seed;
	private static final long multiplier = 0x5DEECE66DL;
	private static final long addend = 0xBL;

	public RandomSequence(int seed) {
		this.seed = seed;
	}

	/**
	 * 对给定数 乱序
	 * 
	 * @return 乱序后的数组
	 */
	public Integer[] getSequence(Integer[] seq) {
		int no = seq.length;
		for (int i = 0; i < no; i++) {
			int p = nextInt(no);
			if (p == i) {
				i--;
				continue;
			}
			seq[i] ^= seq[p];
			seq[p] ^= seq[i];
			seq[i] ^= seq[p];
		}
		return seq;
	}

	private int nextInt(int n) {
		if (n <= 0)
			throw new IllegalArgumentException("n must be positive");
		int bits, val;
		do {
			bits = next(31);
			val = bits % n;// 考虑离散性
		} while (bits - val + (n - 1) < 0);
		return val;
	}

	private int next(int bits) {
		seed = (seed * multiplier + addend);
		return (int) (seed);
	}

}
