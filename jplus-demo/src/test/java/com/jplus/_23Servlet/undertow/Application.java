package com.jplus._23Servlet.undertow;

import javax.servlet.ServletException;

import com.jplus.core.core.abstracts.Environment.FRAME;
import com.jplus.core.mvc.servlet.DispatcherServlet;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.PathHandler;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;

public class Application {

	public static void main(String[] args) throws ServletException {

		String path = "/jplus";
		DeploymentInfo servletBuilder = Servlets.deployment().setClassLoader(Application.class.getClassLoader())
				.setContextPath(path).setDeploymentName(path.replaceAll("/", ""))
				.addServlets(Servlets.servlet("DispatcherServlet", DispatcherServlet.class)
						.addInitParam(FRAME.COMPONENT_SCAN.name(), "com.jplus.mvc").addMapping("/")
						.setLoadOnStartup(1));

		DeploymentManager manager = Servlets.defaultContainer().addDeployment(servletBuilder);
		manager.deploy();

		HttpHandler httpHandler = manager.start();
		PathHandler pathHandler = Handlers.path();

		pathHandler.addPrefixPath(path, httpHandler);
		Undertow server = Undertow.builder().addHttpListener(8080, "localhost").setHandler(pathHandler).build();
		server.start();

	}
}
