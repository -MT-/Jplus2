package com.jplus._22Quartz;

import java.io.IOException;

import com.jplus._22Quartz.config.Config;
import com.jplus.core.core.AnnotationConfigApplicationContext;

/**
 * 集成Quartz
 * 
 * @author yuanqy
 * @time 2017年11月21日 下午4:10:45
 */
public class Application {

	public static void main(String[] args) throws InterruptedException, IOException {
		new AnnotationConfigApplicationContext(Config.class);

		Thread.sleep(1000 * 50);
	}
}
