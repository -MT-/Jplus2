package com.jplus._22Quartz.service;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.anno.boot.Component;
import com.jplus.plugin.quartz.annotation.Quartz;

/**
 * Quartz demo 可以直接指定cron ，也可以读取配置文件
 */
@Component
@Quartz(cron = "${quartz.cron}")
public class QuartzService implements org.quartz.Job {

	private final Logger log = LoggerFactory.getLogger(QuartzService.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info(">> [quartz] 执行定时任务...");

	}

}
