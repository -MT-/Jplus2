package com.jplus._22Quartz.config;

import com.jplus.core.anno.boot.ComponentScan;
import com.jplus.core.anno.boot.Configuration;
import com.jplus.core.anno.boot.PropertySource;
import com.jplus.plugin.quartz.EnableQuartz;

@Configuration
@ComponentScan("com.jplus._22Quartz")
@PropertySource("classpath:prop/22.properties")
@EnableQuartz // 开启Quartz定时任务
public class Config {

}
