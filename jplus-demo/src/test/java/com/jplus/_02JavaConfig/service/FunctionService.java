package com.jplus._02JavaConfig.service;

public class FunctionService {

	private String tell;

	public String sayHello(String word) {
		return "Hello [" + tell + "]" + word;
	}

	public void setTell(String tell) {
		this.tell = tell;
	}

}
