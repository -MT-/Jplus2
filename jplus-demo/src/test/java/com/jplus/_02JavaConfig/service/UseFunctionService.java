package com.jplus._02JavaConfig.service;

public class UseFunctionService {

	private FunctionService server;

	public String sayHello(String word) {
		return server.sayHello(word);
	}

	public void setServer(FunctionService server) {
		this.server = server;
	}

}
