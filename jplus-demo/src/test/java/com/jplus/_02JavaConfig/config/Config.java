package com.jplus._02JavaConfig.config;

import com.jplus._02JavaConfig.service.FunctionService;
import com.jplus._02JavaConfig.service.UseFunctionService;
import com.jplus.core.anno.boot.Bean;
import com.jplus.core.anno.boot.ComponentScan;
import com.jplus.core.anno.boot.Configuration;

@Configuration
@ComponentScan("com.jplus.demo._2JavaConfig")
public class Config {
	@Bean
	public FunctionService functionService() {
		FunctionService bean = new FunctionService();
		bean.setTell("this is JConfig"); // 1.这里有自定义内部操作
		return bean;
	}

	@Bean
	public UseFunctionService useFunctionService(FunctionService functionService) {// 2.这里有其他Bean入参
		UseFunctionService ufs = new UseFunctionService();
		ufs.setServer(functionService);
		return ufs;
	}
}
