package com.jplus._02JavaConfig;

import com.jplus._02JavaConfig.config.Config;
import com.jplus._02JavaConfig.service.UseFunctionService;
import com.jplus.core.core.AnnotationConfigApplicationContext;

public class Application {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
		UseFunctionService ufs = context.getBean(UseFunctionService.class);
		System.out.println(ufs.sayHello("java config"));
	}
}
