package com.jplus._08Event.service;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus._08Event.event.DemoEvent;
import com.jplus.core.anno.com.Autowired;
import com.jplus.core.anno.com.Service;
import com.jplus.core.core.ApplicationContext;
import com.jplus.core.plugin.async.Async;

/**
 * 这是一个普通Service, 使用ApplicationContext发布事件
 */

@Service
public class DemoService {
	private final Logger log = LoggerFactory.getLogger(DemoService.class);
	private @Autowired ApplicationContext context;

	public void doPay(BigDecimal msg) {
		log.info(">> [同步] 支付成功，金额：" + msg);
		context.publishEvent(new DemoEvent(this, msg));
		log.info(">> [同步] 支付完成");
	}

	@Async("myExecutor") // 可以设置具体的执行线程池
	public void asyncMethod() {
		log.info(">>[线程池异步方法] 被@Async 注解的类或方法才会异步");
	}
}
