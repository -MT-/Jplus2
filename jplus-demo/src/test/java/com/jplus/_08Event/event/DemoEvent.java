package com.jplus._08Event.event;

import com.jplus.core.core.events.ApplicationEvent;

public class DemoEvent extends ApplicationEvent {
	private static final long serialVersionUID = 1L;
	private Object msg;

	public DemoEvent(Object source, Object msg) {
		super(source);
		this.msg = msg;
	}

	public Object getMsg() {
		return msg;
	}

	public void setMsg(Object msg) {
		this.msg = msg;
	}

}
