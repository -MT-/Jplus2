package com.jplus._08Event.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.anno.boot.Component;
import com.jplus.core.core.events.ApplicationListener;
import com.jplus.core.plugin.async.Async;

/**
 * 事件监听 + 异步执行
 * 
 * @author yuanqy
 */
@Async
@Component
public class DemoListener implements ApplicationListener<DemoEvent> {
	private final Logger log = LoggerFactory.getLogger(DemoListener.class);

	@Override
	public void onApplicationEvent(DemoEvent event) {
		Object msg = event.getMsg();
		log.info(">>[异步收到消息] 修改账户余额：" + msg);
	}
}
