package com.jplus._08Event;

import java.math.BigDecimal;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.jplus._08Event.service.DemoService;
import com.jplus.core.anno.boot.Bean;
import com.jplus.core.anno.boot.ComponentScan;
import com.jplus.core.anno.boot.Configuration;
import com.jplus.core.core.AnnotationConfigApplicationContext;
import com.jplus.core.plugin.async.EnableAsync;

/**
 * Spring的事件(Application Event) 为Bean与Bean之间的消息通信提供了支持 。 <br>
 * 当一个Bean处理完一个任务后，希望另外一个Bean知道并能做出相应的处理，这时我们需要另外一个Bean监听当前bean所发送的事件。 <br>
 * 流程：
 * <ul>
 * <li>自定义事件，继承ApplicationEvent</li>
 * <li>定义事件监听器，实现ApplicationListener</li>
 * <li>容器发布事件</li>
 * </ul>
 * 
 * @author Yuanqy
 *
 */
@Configuration
@ComponentScan("com.jplus._08Event")
@EnableAsync // 开启异步
public class Application {

	@Bean("myExecutor")
	public ExecutorService executorService() {
		return Executors.newFixedThreadPool(3);// 创建一个定成的线程池
	}

	public static void main(String[] args) throws InterruptedException {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Application.class);
		DemoService dp = context.getBean(DemoService.class);
		dp.doPay(BigDecimal.TEN);
		dp.asyncMethod();
		Thread.sleep(1000);
	}
}
