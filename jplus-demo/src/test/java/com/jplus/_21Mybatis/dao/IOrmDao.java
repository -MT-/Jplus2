package com.jplus._21Mybatis.dao;

import java.util.List;

import com.jplus._21Mybatis.entity.User;

public interface IOrmDao {

	void addUser(User u);

	List<User> listUser();
}
