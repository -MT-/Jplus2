package com.jplus._21Mybatis.service;

import java.util.List;

import com.jplus._21Mybatis.dao.IOrmDao;
import com.jplus._21Mybatis.entity.User;
import com.jplus.core.anno.com.Autowired;
import com.jplus.core.anno.com.Service;

@Service
public class OrmService {

	private @Autowired IOrmDao ormDao;

	public void addUser(User u) {
		ormDao.addUser(u);
	}

	public List<User> listUser() {
		return ormDao.listUser();
	}
}
