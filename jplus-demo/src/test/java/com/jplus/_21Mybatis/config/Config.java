package com.jplus._21Mybatis.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.jplus.core.anno.boot.Bean;
import com.jplus.core.anno.boot.ComponentScan;
import com.jplus.core.anno.boot.Configuration;
import com.jplus.core.anno.boot.PropertySource;
import com.jplus.core.anno.com.Value;
import com.jplus.core.db.TransactionManager;
import com.jplus.core.plugin.transaction.EnableTransaction;
import com.jplus.plugin.mybatis.EnableMybatis;

@Configuration
@ComponentScan("com.jplus._21Mybatis")
@PropertySource("classpath:prop/21.properties")
@EnableMybatis // 开启Mybatis支持
@EnableTransaction // 开启事务支持[选]
public class Config {
	private @Value("${db.url}") String jdbcurl;
	private @Value("${db.username}") String username;
	private @Value("${db.password}") String password;
	private @Value("${db.driver}") String driver;

	@Bean
	public DruidDataSource druidDataSource() {
		DruidDataSource dataSource = new DruidDataSource();
		dataSource.setUrl(jdbcurl);
		dataSource.setDriverClassName(driver);
		dataSource.setUsername(username);
		dataSource.setPassword(password);
		// ==
		dataSource.setRemoveAbandoned(true);
		dataSource.setRemoveAbandonedTimeout(1800);
		// ==
		TransactionManager.addDataSource(dataSource);// 【重要】 注册默认数据源
		return dataSource;
	}

}
