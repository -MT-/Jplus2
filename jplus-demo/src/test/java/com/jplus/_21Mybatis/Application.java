package com.jplus._21Mybatis;

import java.io.IOException;
import java.util.List;

import com.jplus._21Mybatis.config.Config;
import com.jplus._21Mybatis.entity.User;
import com.jplus._21Mybatis.service.OrmService;
import com.jplus.core.core.AnnotationConfigApplicationContext;

/**
 * 集成Mybatis
 * 
 * @author yuanqy
 * @time 2017年11月21日 下午4:10:45
 */
public class Application {

	public static void main(String[] args) throws InterruptedException, IOException {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
		OrmService orm = context.getBean(OrmService.class);

		orm.addUser(new User("张三", "Password"));

		List<User> list = orm.listUser();
		System.out.println(list);

	}
}
