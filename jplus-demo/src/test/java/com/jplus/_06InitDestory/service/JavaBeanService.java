package com.jplus._06InitDestory.service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import com.jplus.core.anno.com.Value;

public class JavaBeanService {
	private @Value("${application.name}") String name;

	public void init() {
		System.out.println("Bean    init()");
		System.out.println("Bean    EL:" + name);
	}

	public void destory() {
		System.out.println("Bean    destory()");
	}

	@PostConstruct
	public void PostConstruct() {
		System.out.println("JSR250  init()");
		System.out.println("JSR250  EL:" + name);
	}

	@PreDestroy
	public void PreDestroy() {
		System.out.println("JSR250  destory()");
	}

	public JavaBeanService() {
		System.out.println("Java    construct()");
	}

	static {
		System.out.println("Java    static{}");
	}

}
