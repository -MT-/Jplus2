package com.jplus._06InitDestory.config;

import com.jplus._06InitDestory.service.JavaBeanService;
import com.jplus.core.anno.boot.Bean;
import com.jplus.core.anno.boot.ComponentScan;
import com.jplus.core.anno.boot.Configuration;
import com.jplus.core.anno.boot.PropertySource;

@Configuration
@ComponentScan("com.jplus._06InitDestory")
@PropertySource("classpath:prop/06.properties")
public class Config {

	@Bean(initMethod = "init", destroyMethod = "destory")
	public JavaBeanService javaBeanService() {
		return new JavaBeanService();
	}

}
