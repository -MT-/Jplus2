package com.jplus._06InitDestory;

import com.jplus._06InitDestory.config.Config;
import com.jplus._06InitDestory.service.JavaBeanService;
import com.jplus.core.core.AnnotationConfigApplicationContext;

public class Application {
	/**
	 * <strong>Bean的初始化与销毁 </strong><br>
	 * 
	 * JavaBean方式： <br>
	 * 使用@Bean的initMethod和destoryMethod【相当于Spring Xml的配置】 <br>
	 * 
	 * 注解方式： <br>
	 * 使用JSR250的@PostConstruct和@PreDestory <br>
	 */
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
		context.getBean(JavaBeanService.class);
	}
}
