package com.jplus._04Scope;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus._04Scope.service.PrototypeService;
import com.jplus._04Scope.service.RequestService;
import com.jplus._04Scope.service.SingleService;
import com.jplus.core.core.AnnotationConfigApplicationContext;

public class Application {
	private static Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) throws InterruptedException {
		final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.jplus._04Scope");
		final SingleService s1 = context.getBean(SingleService.class);
		final SingleService s2 = context.getBean(SingleService.class);

		final PrototypeService p1 = context.getBean(PrototypeService.class);
		final PrototypeService p2 = context.getBean(PrototypeService.class);

		final RequestService r1 = context.getBean(RequestService.class);
		final RequestService r2 = context.getBean(RequestService.class);

		log.info("Scope:Single  \t" + (s1 == s2));
		log.info("Scope:Prototy  \t" + (p1 == p2));
		log.info("Scope:Request  \t" + (r1 == r2));

		Thread t = new Thread() {
			@Override
			public void run() {
				SingleService s3 = context.getBean(SingleService.class);
				log.info("Scope:Single  \t" + (s1 == s3));
				final PrototypeService p3 = context.getBean(PrototypeService.class);
				log.info("Scope:Prototy  \t" + (p1 == p3));
				RequestService r3 = context.getBean(RequestService.class);
				log.info("Scope:Request  \t" + (r1 == r3));
				super.run();
			}
		};
		t.start();
		t.join();
		log.info("Scope:single[单例],prototy[多例],request[当前线程/请求内单例,],session[当前session内单例],globalsession[全局session内单例]");
	}
}
