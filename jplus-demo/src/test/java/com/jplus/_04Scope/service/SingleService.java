package com.jplus._04Scope.service;

import com.jplus.core.anno.com.Scope;
import com.jplus.core.anno.com.Service;
import com.jplus.core.anno.com.Scope.EScope;

/**
 * 单实例
 */
@Service
@Scope(EScope.SINGLETON)
public class SingleService {

}
