package com.jplus._04Scope.service;

import com.jplus.core.anno.com.Scope;
import com.jplus.core.anno.com.Service;
import com.jplus.core.anno.com.Scope.EScope;

/**
 * 每个Request/线程 一个实例
 */
@Service
@Scope(EScope.REQUEST)
public class RequestService {

}
