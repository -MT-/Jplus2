package com.jplus._04Scope.service;

import com.jplus.core.anno.com.Scope;
import com.jplus.core.anno.com.Service;
import com.jplus.core.anno.com.Scope.EScope;

/**
 * 多实例，每次都是新实例
 */
@Service
@Scope(EScope.PROTOTYPE)
public class PrototypeService {

}
