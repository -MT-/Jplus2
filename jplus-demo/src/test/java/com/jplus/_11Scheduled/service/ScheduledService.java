package com.jplus._11Scheduled.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.anno.com.Service;
import com.jplus.core.anno.com.Value;
import com.jplus.core.plugin.async.Async;
import com.jplus.core.plugin.scheduling.Scheduled;

/**
 * 注意，当前Bean是被容器管辖的，所以可以获取文件上下文。 由于是在多线程种执行，所以注意线程安全问题。
 * 
 * @author Yuanqy
 *
 */
@Service
public class ScheduledService {

	private Logger log = LoggerFactory.getLogger(ScheduledService.class);

	private @Value("${task.cron}") String cron;
	private @Value("${method.sleep}") Long sleep;
	private boolean first = true;

	/**
	 * 特别注意。<br>
	 * fixedRate 为限流执行，即每次执行间隔与设定一致，与任务的执行耗时无关。
	 * 默认方法使用单线程执行，当执行耗时超过了设定限流时间，推荐使用线程池执行，否则将照成任务等待
	 * 
	 * @throws InterruptedException
	 */
	@Async("executorService") // 执行异步方法（使用某个线程池）
	@Scheduled(fixedRate = 5000, initialDelay = 6000)
	public void fixedRate() throws InterruptedException {
		log.warn("限流5秒钟执行,任务耗时：" + sleep);
		Thread.sleep(sleep);
	}

	/**
	 * 特别注意：<br>
	 * fixedDelay 为间隔执行，即当前执行时间依据上次结束时间算起，与任务耗时相关，跟随单线程，建议单线程执行。<br>
	 * initialDelay 启动后延迟多久，执行第一次
	 * 
	 */
	@Scheduled(fixedDelay = 5000, initialDelay = 6000)
	public void fixedDelay() throws InterruptedException {
		if (first)
			log.info("首次任务执行,任务耗时：" + sleep);
		else
			log.info("间隔5秒钟执行,任务耗时：" + sleep);
		first = false;
		Thread.sleep(sleep);
	}

	@Scheduled(cron = "0/10 * * * * ? *")
	public void cron() {
		log.info("指定时间Cron执行");
	}

	@Scheduled(cron = "${task.cron}")
	public void cronEl() {
		log.info("自定义时间Cron执行 ");
	}
}
