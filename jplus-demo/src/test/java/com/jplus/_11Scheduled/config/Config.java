package com.jplus._11Scheduled.config;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.jplus.core.anno.boot.Bean;
import com.jplus.core.anno.boot.ComponentScan;
import com.jplus.core.anno.boot.Configuration;
import com.jplus.core.anno.boot.PropertySource;
import com.jplus.core.plugin.async.EnableAsync;
import com.jplus.core.plugin.scheduling.EnableScheduling;

@Configuration
@ComponentScan("com.jplus._11Scheduled")
@PropertySource("classpath:prop/11.properties")
@EnableScheduling // 开启任务调度支持
@EnableAsync // 开启异步
public class Config {

	@Bean
	public ExecutorService executorService() {
		return Executors.newFixedThreadPool(3);// 创建一个定长的线程池
	}

}
