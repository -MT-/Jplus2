package com.jplus._11Scheduled;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.jplus._11Scheduled.config.Config;
import com.jplus.core.core.AnnotationConfigApplicationContext;
import com.jplus.core.plugin.scheduling.cron.CronKit;
import com.jplus.core.utill.JUtil;

public class Application {
	private final static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static void main(String[] args) throws InterruptedException {
		new AnnotationConfigApplicationContext(Config.class);
		Thread.sleep(1000 * 60 * 3);
		Calendar cal = Calendar.getInstance();
		for (int i = 0; i < 60; i++) {
			System.out.println(cal.getTime());
			cal.add(Calendar.DAY_OF_WEEK, 1);
		}

	}

	@org.junit.Test
	public void test_Cron() {
		try {
			System.err.println("Now Time:\n" + SDF.format(new Date()));
			// CronKit cron = new CronKit("1,5 */23 3-5 2/3 * ? *");
			// CronKit cron = new CronKit("1-2 1/2 0,1,4 1-2 * ? * ");
			CronKit cron = new CronKit("0 1-3 1/3 20-31 * THU-SAT 2019");
			System.out.println(JUtil.leftPad(Long.toBinaryString(cron.getCrop(0)), 64, "0") + ":秒 ");
			System.out.println(JUtil.leftPad(Long.toBinaryString(cron.getCrop(1)), 64, "0") + ":分 ");
			System.out.println(JUtil.leftPad(Long.toBinaryString(cron.getCrop(2)), 64, "0") + ":时 ");
			System.out.println(JUtil.leftPad(Long.toBinaryString(cron.getCrop(3)), 64, "0") + ":天 ");
			System.out.println(JUtil.leftPad(Long.toBinaryString(cron.getCrop(4)), 64, "0") + ":月 ");
			System.out.println(JUtil.leftPad(Long.toBinaryString(cron.getCrop(5)), 64, "0") + ":周 ");
			System.out.println(JUtil.leftPad(Long.toBinaryString(cron.getCrop(6)), 64, "0") + ":年 ");
			for (int i = 0; i < 100; i++) {
				System.out.println(SDF.format(cron.next()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
