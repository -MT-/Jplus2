package com.jplus;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jplus.core.mvc.reqeust.RequestMethod;

public class JSONTest {
	static final Logger log = LoggerFactory.getLogger(JSONTest.class);

	public static void main(String[] args) {

		String jsonStr = "{\"money\":56.39,\"_$name\":\"命名\",\"score\":45.6,\"time\":15618387360,\"id\":100001,\"list\":[\"GET\",\"PUT\",\"POST\"],\"day\":\"2019-09-18 14:23:00\",\"map\":{\"key1\":\"value1\",\"key2\":\"value2\"},\"age\":27,\"bo\":true,\"generic\":\"54321\",\"req\":\"POST\",\"child\":{\"money\":23.1,\"time\":15618387366,\"_$name\":\"命名\",\"score\":78.99,\"id\":1002,\"list\":[\"GET\",\"POST\"],\"day\":\"2019-09-18 14:23:00\",\"map\":null,\"age\":26,\"bo\":false,\"generic\":\"12345\",\"req\":\"GET\",\"child\":null}}";

		com.alibaba.fastjson.JSON.DEFFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
		com.jplus.core.utill.JSON.setDefDateFormat("yyyy-MM-dd HH:mm:ss");

		log.info("格式化:\n" + com.jplus.core.utill.JSON.format(jsonStr));

		Data<String> jsonBean1 = com.jplus.core.utill.JSON.parseBean(jsonStr,
				new com.jplus.core.utill.JSON.TypeReference<Data<String>>() {
				});
		Data<String> jsonBean2 = com.jplus.core.utill.JSON.parseBean(jsonStr,
				new com.jplus.core.utill.JSON.TypeReference<Data<String>>() {
				});

		log.info(jsonStr);

		final int count = 1000;// 执行次数
		log.warn("Serialization：");
		// ==JSON
		{
			long t = System.currentTimeMillis();
			for (int i = count; i >= 0; i--) {
				jsonBean1.id += 1;
				String temp = com.jplus.core.utill.JSON.toJSONString(jsonBean1);
				if (i == 0) {
					log.info("[TIME]:" + (System.currentTimeMillis() - t));
					log.info("[JplusJSON]:\t" + temp);
				}
			}
		}
		// ==FastJSON
		{
			long t = System.currentTimeMillis();
			for (int i = count; i >= 0; i--) {
				jsonBean1.id += 1;
				String temp = com.alibaba.fastjson.JSON.toJSONString(jsonBean1,
						SerializerFeature.WriteDateUseDateFormat, SerializerFeature.WriteMapNullValue);
				if (i == 0) {
					log.info("[TIME]:" + (System.currentTimeMillis() - t));
					log.info("[FastJSON]:\t" + temp);
				}
			}
		}
		log.warn("Deserialization：");
		// ==JSON
		{
			long t = System.currentTimeMillis();
			for (int i = count; i >= 0; i--) {
				Data<String> temp = com.jplus.core.utill.JSON.parseBean(jsonStr,
						new com.jplus.core.utill.JSON.TypeReference<Data<String>>() {
						});
				if (i == 0) {
					log.info("[TIME]:" + (System.currentTimeMillis() - t));
					log.info("[JplusJSON]:\t" + temp);
				}
			}
		}
		// ==FastJSON
		{
			long t = System.currentTimeMillis();
			for (int i = count; i >= 0; i--) {
				Data<String> temp = com.alibaba.fastjson.JSON.parseObject(jsonStr,
						new com.alibaba.fastjson.TypeReference<Data<String>>() {
						});
				if (i == 0) {
					log.info("[TIME]:" + (System.currentTimeMillis() - t));
					log.info("[FastJSON]:\t" + temp);
				}
			}
		}

	}

	/**
	 * 测试对象
	 */
	public static class Data<T> {
		public int id;
		public Integer age;
		public Long time;
		public float score;
		public BigDecimal money;
		public java.util.Date day;
		public RequestMethod req;
		public List<RequestMethod> list;
		public Map<String, String> map;
		public Data<T> child;
		public T generic;
		@JSONField(name = "_$name")
		public String _$name;
		public boolean bo;

		@Override
		public String toString() {
			return "{id=" + id + ", age=" + age + ", time=" + time + ", money=" + money + ", day=" + day + ", req="
					+ req + ", list=" + list + ", map=" + map + ", child=" + child + ", generic=" + generic
					+ ", _$name=" + _$name + ", bo=" + bo + "}";
		}

	}
}
