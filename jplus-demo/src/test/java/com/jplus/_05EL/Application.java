package com.jplus._05EL;

import com.jplus._05EL.config.Config;
import com.jplus._05EL.service.DemoService;
import com.jplus.core.core.AnnotationConfigApplicationContext;

public class Application {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
		Config con = context.getBean(Config.class);
		con.outputResource();

		DemoService ds = context.getBean(DemoService.class);
		System.out.println("getAuthor:" + ds.getAuthor());
	}
}
    