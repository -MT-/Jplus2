package com.jplus._05EL.service;

import com.jplus.core.anno.com.Service;
import com.jplus.core.anno.com.Value;

@Service
public class DemoService {
	@Value("${application.author}")
	private String author;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

}
