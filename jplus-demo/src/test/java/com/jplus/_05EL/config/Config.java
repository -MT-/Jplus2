package com.jplus._05EL.config;

import com.jplus._05EL.service.DemoService;
import com.jplus.core.anno.boot.Bean;
import com.jplus.core.anno.boot.ComponentScan;
import com.jplus.core.anno.boot.Configuration;
import com.jplus.core.anno.boot.PropertySource;
import com.jplus.core.anno.com.Value;

@Configuration
@ComponentScan("com.spring.demo._05EL.service")
@PropertySource("classpath:prop/05.properties")
public class Config {
	@Value("I love you")
	public String normal;// 注入：普通字符串

	@Value("${os.name}")
	public String osName;// 注入：系统配置信息

	@Value("#{60*60*24}")
	public Long arithmetic;// 注入：算数表达式

	@Value("${application.name}")
	public String properties;// 注入：配置文件

	@Bean
	public DemoService DemoService() {
		return new DemoService();
	}

	public void outputResource() {
		/**
		 * 在SpringBoot中，可以使用如：
		 * 
		 * @ConfigurationProperties(prefix = "spring.redis"); 作为配置映射类
		 */

		System.out.println("【normal】" + normal);
		System.out.println("【osName】" + osName);
		System.out.println("【arithmetic】" + arithmetic);
		System.out.println("【properties】" + properties);
	}
}
