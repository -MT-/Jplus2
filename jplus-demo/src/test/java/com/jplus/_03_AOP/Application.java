package com.jplus._03_AOP;

import com.jplus._03_AOP.config.Config;
import com.jplus._03_AOP.service.FunctionService;
import com.jplus.core.core.AnnotationConfigApplicationContext;

public class Application {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
		FunctionService ufs = context.getBean(FunctionService.class);
		System.err.println(ufs.sayHello("AOP"));
	}
}
