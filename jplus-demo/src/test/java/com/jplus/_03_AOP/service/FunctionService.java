package com.jplus._03_AOP.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.anno.boot.Component;

@Component
public class FunctionService {

	private final Logger log = LoggerFactory.getLogger(FunctionService.class);

	public String sayHello(String word) {
		String str = "Hello  " + word;
		log.info("【方法内部】 {}", str);
		return str;
	}

}
