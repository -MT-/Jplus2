package com.jplus._03_AOP.config;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.anno.boot.ComponentScan;
import com.jplus.core.anno.boot.Configuration;
import com.jplus.core.core.proxys.ProxyChain;
import com.jplus.core.plugin.aspect.Aspect;
import com.jplus.core.plugin.aspect.EnableAspect;
import com.jplus.core.plugin.aspect.MethodInterceptor;

/**
 * 对拦截类Bean对象加上@Aspect注解，并继承AspectPlugin抽象类即可。
 */
@Configuration
@ComponentScan("com.jplus._03_AOP")
@EnableAspect
@Aspect("* com.jplus._03_AOP.service.FunctionService.*(..)")
public class Config extends MethodInterceptor {

	private final Logger log = LoggerFactory.getLogger(Config.class);

	@Override
	public void begin() {
		log.info("[begin]>> 方法执行前-最开始");
	}

	@Override
	public boolean intercept(Class<?> cls, Method method, Object[] params) throws Throwable {
		log.info("[intercept]>> 方法执行前阻塞[true：执行/false:不执行]");
		return super.intercept(cls, method, params);
	}

	@Override
	public void before(Class<?> cls, Method method, Object[] params) throws Throwable {
		log.info("[before]>> 即将执行方法体，异常能被error()方法捕获");
		super.before(cls, method, params);
	}

	@Override
	public Object around(ProxyChain proxyChain) throws Throwable {
		log.info("[around]>>A [环绕]执行前");
		Object obj = super.around(proxyChain);
		log.info("[around]>>B [环绕]执行后");
		return obj;
	}

	@Override
	public void after(Class<?> cls, Method method, Object[] params, Object result) throws Throwable {
		log.info("[after]>> 执行完方法体，，异常能被error()方法捕获");
		super.after(cls, method, params, result);
	}

	@Override
	public void error(Class<?> cls, Method method, Object[] params, Throwable e) {
		System.err.println("[error]>> 捕获从intercept > after 整个流程的异常，包括方法体");
		super.error(cls, method, params, e);
	}

	@Override
	public void end() {
		log.info("[end]>> 执行结束 finally");
		super.end();
	}

}
