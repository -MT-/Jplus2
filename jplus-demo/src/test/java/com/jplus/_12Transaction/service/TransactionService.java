package com.jplus._12Transaction.service;

import java.sql.SQLException;

import javax.annotation.PostConstruct;

import com.jplus.core.anno.com.Autowired;
import com.jplus.core.anno.com.Service;
import com.jplus.core.db.JdbcTemplate;
import com.jplus.core.plugin.transaction.Transaction;

@Service
public class TransactionService {
	@Autowired
	public JdbcTemplate jdbctemplate;

	@Transaction // 可用于方法或类上
	public int executeOK() throws SQLException {
		doSql("[TransactionTest]Do Succ");
		return 10 / 10;
	}

	@Transaction // 可用于方法或类上
	public int executeFail() throws SQLException {
		doSql("[TransactionTest]Do Fail");
		return 10 / 0; // --抛异常
	}

	@PostConstruct
	private void doSql(Object... param) throws SQLException {
		jdbctemplate.execute("INSERT INTO demo (name) VALUES (?)", param);
	}

}
