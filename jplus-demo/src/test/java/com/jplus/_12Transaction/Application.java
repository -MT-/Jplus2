package com.jplus._12Transaction;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus._12Transaction.config.Config;
import com.jplus._12Transaction.service.TransactionService;
import com.jplus.core.core.AnnotationConfigApplicationContext;

public class Application {
	private static Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) throws SQLException, InterruptedException {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);

		TransactionService transaction = context.getBean(TransactionService.class);
		try {
			logger.error(">>executeOK:");
			transaction.executeOK();
			logger.error(">>executeFail:");
			transaction.executeFail();
		} catch (Exception e) {
			logger.error("[DB ERROR]: 故意抛异常{}", e);
		}
		Thread.sleep(1000);

	}

}
