package com.jplus._12Transaction.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.jplus.core.anno.boot.Bean;
import com.jplus.core.anno.boot.ComponentScan;
import com.jplus.core.anno.boot.Configuration;
import com.jplus.core.anno.boot.PropertySource;
import com.jplus.core.anno.com.Value;
import com.jplus.core.db.TransactionManager;
import com.jplus.core.db.JdbcTemplate;
import com.jplus.core.plugin.transaction.EnableTransaction;

@Configuration
@ComponentScan("com.jplus._12Transaction")
@PropertySource("classpath:prop/12.properties")
@EnableTransaction // 开启事务控制
public class Config {

	private @Value("${db.url}") String jdbcurl;
	private @Value("${db.username}") String username;
	private @Value("${db.password}") String password;
	private @Value("${db.driver}") String driver;

	@Bean
	public DruidDataSource druidDataSource1() {
		DruidDataSource dataSource = new DruidDataSource();
		dataSource.setUrl(jdbcurl);
		dataSource.setDriverClassName(driver);
		dataSource.setUsername(username);
		dataSource.setPassword(password);
		// ==
		dataSource.setRemoveAbandoned(true);
		dataSource.setRemoveAbandonedTimeout(1800);
		// ==
		TransactionManager.addDataSource(dataSource);// 【重要】注册数据源
		return dataSource;
	}

	@Bean
	public JdbcTemplate jdbcTemplate(DruidDataSource druidDataSource) throws ClassNotFoundException {
		return TransactionManager.getJdbcTemplate();
	}
}
