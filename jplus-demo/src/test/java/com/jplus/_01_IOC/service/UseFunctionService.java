package com.jplus._01_IOC.service;

import com.jplus.core.anno.com.Autowired;
import com.jplus.core.anno.com.Service;

@Service
public class UseFunctionService {

	// 1.属性注入
	@Autowired
	private FunctionService server1;
	private FunctionService server2;

	public UseFunctionService(FunctionService server) {
		this.server2 = server;// 2.构造方法注入
	}

	public String sayHello(String word) {
		return server1.sayHello("[属性]" + word) + server2.sayHello("[构造]" + word);
	}

}
