package com.jplus._01_IOC.service;

import com.jplus.core.anno.com.Service;

@Service("newName")
public class FunctionService {

	public String sayHello(String word) {
		return "Hello " + word;
	}
}
