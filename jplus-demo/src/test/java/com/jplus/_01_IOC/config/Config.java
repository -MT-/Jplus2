package com.jplus._01_IOC.config;

import com.jplus.core.anno.boot.ComponentScan;
import com.jplus.core.anno.boot.Configuration;

@Configuration()
@ComponentScan("com.jplus._01_IOC")
public class Config {

}
