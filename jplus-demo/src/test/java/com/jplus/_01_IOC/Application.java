package com.jplus._01_IOC;

import com.jplus._01_IOC.config.Config;
import com.jplus._01_IOC.service.FunctionService;
import com.jplus._01_IOC.service.UseFunctionService;
import com.jplus.core.core.AnnotationConfigApplicationContext;

public class Application {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
		UseFunctionService ufs = context.getBean(UseFunctionService.class);
		System.out.println(ufs.sayHello("DI/IOC"));

		FunctionService fs = (FunctionService) context.getBean("newName");
		System.out.println(fs.sayHello("自定义BeanName"));
	}
}
