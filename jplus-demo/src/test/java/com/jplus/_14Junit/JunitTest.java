package com.jplus._14Junit;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.jplus._14Junit.config.Config;
import com.jplus.core.anno.com.Value;
import com.jplus.core.junit.JUnit4Runner;
import com.jplus.core.junit.annotation.ContextConfiguration;

@RunWith(JUnit4Runner.class)
@ContextConfiguration(classes = Config.class)
public class JunitTest {

	@Value("${tell.yourself}")
	private String tell;

	@Test
	public void test() {
		System.out.println("JunitTest:" + tell);
	}
}
