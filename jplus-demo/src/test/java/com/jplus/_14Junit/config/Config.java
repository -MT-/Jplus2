package com.jplus._14Junit.config;

import com.jplus.core.anno.boot.ComponentScan;
import com.jplus.core.anno.boot.Configuration;
import com.jplus.core.anno.boot.PropertySource;

@Configuration
@ComponentScan("com.jplus._14Junit")
@PropertySource("classpath:prop/14.properties")
public class Config {

}
