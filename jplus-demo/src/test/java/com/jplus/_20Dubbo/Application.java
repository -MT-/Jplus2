package com.jplus._20Dubbo;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.dubbo.rpc.service.EchoService;
import com.alibaba.fastjson.JSON;
import com.jplus._20Dubbo.config.Config;
import com.jplus._20Dubbo.config.DubboHandle;
import com.jplus._20Dubbo.service.ConsumeClient;
import com.jplus._20Dubbo.service.IService;
import com.jplus._20Dubbo.service.ProviderImpl;
import com.jplus._20Dubbo.service.bean.StudentUser;
import com.jplus._20Dubbo.service.bean.User;
import com.jplus.core.core.AnnotationConfigApplicationContext;

/**
 * 集成Dubbo
 * 
 * @see http://dubbo.apache.org/zh-cn/docs/user/quick-start.html
 * 
 * @author yuanqy
 * @time 2017年11月21日 下午4:10:45
 */
public class Application {
	private static Logger log = LoggerFactory.getLogger(Application.class);

	@SuppressWarnings("resource")
	public static void main(String[] args) throws InterruptedException, IOException {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
		log.warn("===================== Build Dubbo =====================");
		DubboHandle handle = context.getBean(DubboHandle.class);
		handle.buildSrviceConfig(IService.class, context.getBean(ProviderImpl.class)); // 发布服务端
		handle.buildReferenceConfig(IService.class);// 创建客户端
		log.warn("===================== Execute Test =========================");
		EchoService echo = (EchoService) context.getBean(IService.class);// 回声测试
		log.info("[Dubbo Test] Echo:" + echo.$echo("Connection Succ!"));

		ConsumeClient client = context.getBean(ConsumeClient.class);
		log.info("[Dubbo Result]String:" + client.sendMsg("Hello"));

		List<User> user = client.beanAbstract(new StudentUser(1001L, "張三", new BigDecimal("98.00")));
		log.info("[Dubbo Result]Abstract" + JSON.toJSONString(user));

		User u = client.beanGeneric(new StudentUser(2001L, "張三", new BigDecimal("98.00")));
		log.info("[Dubbo Result]Generic" + JSON.toJSONString(u));

		System.out.println(new Scanner(System.in).nextLine());
		log.warn("===================== Destory =========================");
	}
}
