package com.jplus._20Dubbo.service.bean;

import java.io.Serializable;

/**
 * @author yuanqy
 * @time 2017年12月6日 下午5:11:05
 */
public abstract class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
