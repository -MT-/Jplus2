package com.jplus._20Dubbo.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.jplus._20Dubbo.service.bean.User;
import com.jplus.core.anno.boot.Component;
import com.jplus.core.anno.com.Autowired;
import com.jplus.core.anno.com.Lazy;

/**
 * @author yuanqy
 * @time 2017年12月6日 下午4:50:05
 */
@Component
@Lazy
// 由于启动后注入dubbo客户端，所以当前类要延迟加载。而且当前类不应该有初始化方法，比如:initMethod ,@PostConstruct
public class ConsumeClient {
	private Logger log = LoggerFactory.getLogger(ConsumeClient.class);

	// 这里注入的是dubbo客户端
	private @Autowired IService service;

	public String sendMsg(String msg) {
		log.info("[DUBBO Consumer]：sendMsg=" + msg);
		return service.sendMsg(msg);
	}

	public List<User> beanAbstract(User user) {
		log.info("[DUBBO Consumer]：sendMsg=" + JSON.toJSONString(user));
		return service.bean_abstract(user);
	}

	public User beanGeneric(User user) {
		log.info("[DUBBO Consumer]：sendMsg=" + JSON.toJSONString(user));
		return service.bean_generic(user);
	}
}
