package com.jplus._20Dubbo.service;

import java.util.List;

import com.jplus._20Dubbo.service.bean.User;

/**
 * @author yuanqy
 * @time 2017年11月21日 下午4:17:43
 */
public interface IService {

	/**
	 * 普通测试
	 */
	public String sendMsg(String msg);

	/**
	 * 测试抽象对象传递
	 */
	public List<User> bean_abstract(User user);

	/**
	 * 测试泛型对象传递
	 */
	public <T extends User> T bean_generic(T user);

}
