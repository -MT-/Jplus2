package com.jplus._20Dubbo.service.bean;

import java.math.BigDecimal;

/**
 * @author yuanqy
 * @time 2017年12月6日 下午5:12:36
 */
public class StudentUser extends User {

    private static final long serialVersionUID = 1L;

    private BigDecimal score;

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public StudentUser(Long id, String name, BigDecimal score) {
        super.setId(id);
        super.setName(name);
        this.score = score;
    }

    /**
     * 
     */
    public StudentUser() {
        super();
    }



}
