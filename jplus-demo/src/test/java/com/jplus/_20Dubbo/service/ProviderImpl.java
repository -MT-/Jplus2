package com.jplus._20Dubbo.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.jplus._20Dubbo.service.bean.StudentUser;
import com.jplus._20Dubbo.service.bean.User;
import com.jplus.core.anno.com.Service;

/**
 * @author yuanqy
 * @time 2017年11月21日 下午4:16:26
 */
@Service
public class ProviderImpl implements IService {
	private Logger log = LoggerFactory.getLogger(ProviderImpl.class);

	@PostConstruct
	public void init() {
		log.info(" [init]ProviderImpl");
	}

	@Override
	public String sendMsg(String msg) {
		log.info("[DUBBO Provider]：getParm=" + msg);
		return "OK";
	}

	@Override
	public List<User> bean_abstract(User user) {
		log.info("[DUBBO Provider]：getParm=" + JSON.toJSONString(user));
		List<User> list = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			user.setId(user.getId() + 1);
			list.add(JSON.parseObject(JSON.toJSONString(user), StudentUser.class));
		}
		return list;
	}

	@Override
	public <T extends User> T bean_generic(T user) {
		log.info("[DUBBO Provider]：getParm=" + JSON.toJSONString(user));
		user.setId(user.getId() + 1);
		return user;
	}

}
