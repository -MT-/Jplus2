package com.jplus._20Dubbo.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ConsumerConfig;
import com.alibaba.dubbo.config.ProtocolConfig;
import com.alibaba.dubbo.config.ProviderConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.jplus.core.anno.boot.Bean;
import com.jplus.core.anno.boot.ComponentScan;
import com.jplus.core.anno.boot.Configuration;
import com.jplus.core.anno.boot.PropertySource;
import com.jplus.core.anno.com.Value;
import com.jplus.core.core.ApplicationContext;
import com.jplus.core.utill.JUtil;

/**
 * @author yuanqy
 * @time 2017年11月21日 下午4:11:57
 */
@Configuration
@ComponentScan("com.jplus._20Dubbo")
@PropertySource("classpath:prop/20.properties")
public class Config {
	protected Logger log = LoggerFactory.getLogger(Config.class);
	protected ApplicationContext applicationContext;
	private @Value("${dubbo.application.name}") String dubboAppName;

	private @Value("${dubbo.registry.address}") String dubboRegAddr;
	private @Value("${dubbo.registry.user}") String dubboRegUser;
	private @Value("${dubbo.registry.pwd}") String dubboRegPwd;

	private @Value("${dubbo.protocol.name}") String dubboProName;
	private @Value("${dubbo.protocol.port}") int dubboProPort;

	@Bean
	public ApplicationConfig applicationConfig() {
		log.info(" [init]ApplicationConfig\t=" + dubboAppName);
		// 当前应用配置
		ApplicationConfig application = new ApplicationConfig();
		application.setName(dubboAppName);
		return application;
	}

	@Bean
	public RegistryConfig registryConfig() {
		log.info(" [init]RegistryConfig\t=" + dubboRegAddr);
		// 连接注册中心配置
		RegistryConfig registry = new RegistryConfig();
		registry.setAddress(dubboRegAddr);
		if (!JUtil.isEmpty(dubboRegUser)) {
			registry.setUsername(dubboRegUser);
			registry.setPassword(dubboRegPwd);
		}
		return registry;
	}

	@Bean
	public ProtocolConfig protocolConfig() {
		log.info(" [init]ProtocolConfig\t=" + dubboProPort);
		// 服务提供者协议配置
		ProtocolConfig protocol = new ProtocolConfig();
		protocol.setName(dubboProName);
		protocol.setPort(dubboProPort);
		protocol.setThreads(200);
		return protocol;
	}

	@Bean
	public ProviderConfig providerConfig(ApplicationConfig application, RegistryConfig registry,
			ProtocolConfig protocol) {
		log.info(" [init]ProviderConfig");
		ProviderConfig provider = new ProviderConfig();
		provider.setApplication(application);
		provider.setRegistry(registry);
		provider.setProtocol(protocol);
		return provider;
	}

	@Bean
	public ConsumerConfig consumerConfig(ApplicationConfig application, RegistryConfig registry) {
		log.info(" [init]ConsumerConfig");
		ConsumerConfig consumer = new ConsumerConfig();
		consumer.setApplication(application);
		consumer.setRegistry(registry);
		return consumer;

	}

}
