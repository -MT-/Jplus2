package com.jplus.mvc.action;

import com.jplus.core.mvc.anno.RequestMapping;
import com.jplus.core.mvc.anno.RestController;
import com.jplus.core.mvc.vo.Model;

@RestController
public class IndexAction {

	@RequestMapping("/hello")
	public Model hello() {
		return new Model().SUCC().DATA("Welcome JPlus");
	}

}
