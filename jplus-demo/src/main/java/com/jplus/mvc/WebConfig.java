package com.jplus.mvc;

import com.jplus.core.anno.boot.ComponentScan;
import com.jplus.core.anno.boot.Configuration;

@Configuration
@ComponentScan("com.jplus.mvc")
public class WebConfig {

}
