package com.jplus.mvc;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.mvc.AnnotationConfigWebApplicationContext;
import com.jplus.core.mvc.servlet.DispatcherServlet;
import com.jplus.core.mvc.servlet.WebApplicationInitializer;

/**
 * 
 * 【核心1】Spring的方式启动web
 * 
 * @author Yuanqy
 *
 */
public class WebInitializer implements WebApplicationInitializer {

	private final Logger logger = LoggerFactory.getLogger(WebInitializer.class);

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.register(WebConfig.class);
		context.setServletContext(servletContext);

		context.setDefaultServletMapping("*.html"); // 默认资源
		// context.setDefaultServletMapping("/static/");

		Dynamic servlet = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
		servlet.addMapping("/");
		servlet.setAsyncSupported(true);// 开启Servlet3.0+ 异步支持，Servlet容器也得配置支持
		servlet.setLoadOnStartup(1);
		logger.info("=================== Init start =====================");

	}
}
