package com.jplus.plugin.quartz.bean;

/**
 * JobDetailDto
 * @author Yuanqy
 *
 */
public class JobDetailDto {
	private String name;
	private String group;
	private Class<?> clas;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public Class<? > getClas() {
		return clas;
	}

	public void setClas(Class<?> clas) {
		this.clas = clas;
	}

	public JobDetailDto(String name, String group, Class<?> cla) {
		super();
		this.name = name;
		this.group = group;
		this.clas = cla;
	}

	public JobDetailDto(String name, Class<?> clas) {
		super();
		this.name = name;
		this.group = "default_job_group";
		this.clas = clas;
	}
	

}
