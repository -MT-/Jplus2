package com.jplus.plugin.quartz.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Quartz定时器注解
 * 
 * @author Yuanqy
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Quartz {
	String jobName() default "DefJobName";

	String jobGroup() default "DefJob";

	String triggerName() default "DefTriggerName";

	String triggerGroup() default "DefTriggerGroup";

	/**
	 * cron 表达式
	 * 
	 * <pre>
	 *  &#64;JTask(cron = "task.cron") //支持读取配置信息
	 * </pre>
	 * 
	 * <pre>
	 *  &#64;JTask(cron = "0 0/10 * * * ?") //正常的时间配置事例
	 * </pre>
	 */
	String cron();

}
