package com.jplus.plugin.quartz.bean;

/**
 * SchedulerDto
 * @author Yuanqy
 *
 */
public class SchedulerDto {

	private CronTriggerDto trigger;
	private JobDetailDto jobdetail;

	public CronTriggerDto getTrigger() {
		return trigger;
	}

	public void setTrigger(CronTriggerDto trigger) {
		this.trigger = trigger;
	}

	public JobDetailDto getJobdetail() {
		return jobdetail;
	}

	public void setJobdetail(JobDetailDto jobdetail) {
		this.jobdetail = jobdetail;
	}

	public SchedulerDto(CronTriggerDto trigger, JobDetailDto jobdetail) {
		super();
		this.trigger = trigger;
		this.jobdetail = jobdetail;
	}

	public SchedulerDto() {
		super();
	}
}
