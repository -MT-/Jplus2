package com.jplus.plugin.mybatis;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jplus.core.anno.boot.Import;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(MybatisConfigurationSelector.class)
public @interface EnableMybatis {

	public static enum MYBATIS {
		DAO_PACKAGE("frame.plugin.mybatis.daoPackage"), 
		XML_PATH("frame.plugin.mybatis.xmlPath");

		private String value;

		private MYBATIS(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

	}
}