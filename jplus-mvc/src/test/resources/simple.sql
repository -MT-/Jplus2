-- ################## SAAS系统 #########################
-- ID:listRoom
SELECT  
        trs.psname '分局',
        trs.lpsname '派出所',
        trs.pdname '警区',		
        trs.areaname '区县',
        trs.streetname '街道',
        trs.communityname '社区',
        trr.communityName '小区名称',
        CONCAT(trr.communityName, trr.buildingNo, trr.unitNo, trr.floor, '层', trr.roomNo) '详细地址',
        trr.buildingNo '楼栋',
        trr.floor, '楼层',
        trr.roomNo '房间号',
        tdd.id '门锁ID',
        tdd.lockmac 'MAC',
        CASE tdd.lockType WHEN 1 THEN '联寓'
                WHEN 2 THEN '科技侠'
                WHEN 3 THEN '800门锁'
                WHEN 4 THEN '榉树'
                WHEN 5 THEN '丁盯'
                WHEN 6 THEN '安全榉树'
                WHEN 7 THEN '久禅'
                WHEN 8 THEN '指芯'
                ELSE tdd.lockType
                END '门锁品牌',
        IF(tdd.`enable` = 1, '有效', '无效') '门锁状态',
        tdd.createtime '安装时间'
FROM prd_doorlock_rds.t_dfc_door_lock tdd
INNER JOIN prd_room_rds.t_room_register trr ON tdd.areaType = 3  AND tdd.relationId = trr.id   
LEFT JOIN (SELECT lpsname,pdname,xqName,areaname,streetname,psname,communityname FROM prd_room_rds.t_room_resource_record WHERE `status` = 2  GROUP BY xqName) trs
        ON trr.communityName = trs.xqName
WHERE tdd.`enable` = 1
        AND tdd.relationId = trr.id
        AND trr.orgId = 150342
        AND  trr.svcCenterId NOT IN(1644,1631)
ORDER BY trs.lpsname,trr.communityid 

-- ID:listSaasAuth
SELECT p.*
from prd_doorlock_rds.t_dfc_door_lock l
LEFT JOIN prd_doorlock_rds.t_dfc_doorlockprivilege p on p.doorlockId=l.id
where l.platformType=1 and p.`status`=1
and p.endTime>='2040-01-01 00:00:00'

-- ################## 门锁平台 #########################
-- ID:listPower
SELECT * FROM `t_door_lock_power`  WHERE  id IN (
	SELECT   MAX(id)   FROM `t_door_lock_power` WHERE 1=1
--	  AND lockMac='3C:21:27:05:19:20'
	AND STATUS=1
	GROUP BY lockMac
)

-- ID:listTime
SELECT *,ABS((deviceTime-serverTime)/60)  'xTime' FROM t_door_lock_timing WHERE  id IN ( 
	SELECT   MAX(id)   FROM `t_door_lock_timing` WHERE 1=1
--	  AND lockMac='3C:21:27:05:19:20'
	AND STATUS=1
	GROUP BY lockMac
)

-- ID:listTimeTask
SELECT *,ABS((deviceTime-serverTime)/60)  'xTime' FROM t_door_lock_timing WHERE  id IN (
	SELECT   MAX(id)   FROM `t_door_lock_timing` WHERE 1=1
	AND STATUS=1
	GROUP BY lockMac
)HAVING ABS((deviceTime-serverTime)/60) >10
 
-- ID:querySaasUserByUUID
SELECT iotrefcustomerid,`name`  FROM  prd_saas_rds.t_custom_register WHERE iotrefcustomerid=? limit 1

-- ID:queryLookUserRepeat
SELECT * FROM `t_door_lock_user` WHERE mobile IN (SELECT mobile FROM t_door_lock_user GROUP BY mobile HAVING COUNT(mobile) >1)

-- ID:listSecretKey
SELECT * FROM `t_door_lock_secret_key` WHERE lockMac=?

-- ID:listXTime
SELECT DISTINCT a.lockId,a.lockMac,a.deviceTime,a.serverTime,a.gmtCreate,(a.serverTime-a.deviceTime)/60 'xTime' 
,b.lockBrand,c.lockBrandName
-- ,d.lockAddr
FROM t_door_lock_timing a 
INNER JOIN  (
	SELECT MAX(id) 'maxid',lockMac FROM t_door_lock_timing
	WHERE deviceTime>0
	GROUP BY lockMac
) t ON a.id =t.maxid
INNER JOIN `t_door_lock_basic` b ON a.lockId=b.lockId AND b.status=1 AND b.lockStatus=1
LEFT JOIN `t_door_lock_brand` c ON b.lockBrand=c.lockBrandNo
-- left join `t_door_lock_user_ref` d on a.lockMac=d.lockMac
WHERE (a.serverTime-a.deviceTime)/60>?


-- ################## 润生活 ###########################
-- ID:listPersonPicAdd
-- 查询已入住的
SELECT                                                                                                 
CONCAT(SUBSTRING(t2.absolutePath,15),'/',t2.picName)  AS cardPic ,                                     
CONCAT(SUBSTRING(t3.absolutePath,15),'/',t3.picName)  AS facePic ,                                     
t1.idNo,t1.customerId,t1.customerName,                                                                 
t4.name,t4.mobile,t4.sex,t1.createTime                                                                 
FROM `t_custom_authen_record` t1                                                                       
LEFT JOIN t_custom_register t4 ON t1.customerId=t4.personId                                            
LEFT JOIN `t_basic_pictures` t2 ON t1.`idPackId`=t2.packageId  AND t2.piclevel=1                       
LEFT JOIN `t_basic_pictures` t3 ON t1.`customerPicBagId`=t3.packageId  AND t3.piclevel=1               
WHERE 1=1                                                                                              
AND t1.authenResult=1                                                                                  
-- AND t1.`customerName` IN ('沈涛','龚婷婷')                                                                         
AND t1.customerId   IN  (                                                                              
	SELECT DISTINCT a.customerId FROM `t_custom_users` a 
        INNER JOIN `t_custom_tenant` b ON a.`id`=b.`userId`  
	AND b.`status`=1                                                                               
	AND b.`bizOrgId`=38                                                                            
	AND a.createTime > ?                                                               
)                            
                                                                          
-- ID:listPersonPicDel
-- 查询已退房的
SELECT                                                                                           
	CONCAT(SUBSTRING(t2.absolutePath,15),'/',t2.picName)  AS cardPic ,                               
	CONCAT(SUBSTRING(t3.absolutePath,15),'/',t3.picName)  AS facePic ,                               
	t1.idNo,t1.customerId,t1.customerName,                                                           
	t4.name,t4.mobile,t4.sex,t1.createTime                                                           
FROM `t_custom_authen_record` t1                                                                 
LEFT JOIN t_custom_register t4 ON t1.customerId=t4.personId                                      
LEFT JOIN `t_basic_pictures` t2 ON t1.`idPackId`=t2.packageId  AND t2.piclevel=1                 
LEFT JOIN `t_basic_pictures` t3 ON t1.`customerPicBagId`=t3.packageId  AND t3.piclevel=1         
WHERE 1=1                                                                                        
AND t1.authenResult=1                                                                            
AND t1.customerId   IN  (                                                                        
	SELECT DISTINCT customerId   FROM `t_custom_users` a                                     
	INNER JOIN `t_custom_tenant` b ON a.`id`=b.`userId`                                      
	AND b.`bizOrgId`=38                                                                      
	AND b.`endTime`> ?                                                           
	AND a.`customerId` NOT IN (                                                              
		 SELECT DISTINCT customerId  FROM `t_custom_users` a                             
		 LEFT JOIN t_custom_tenant b  ON a.`id`=b.`userId`                               
		 WHERE b.status=1 AND b.`bizOrgId`=38                                            
	)                                                                                        
)      

