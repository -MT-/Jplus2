package test;

import java.io.File;
import java.io.IOException;

import com.jplus.core.utill.HttpClientUtil;
import com.jplus.core.utill.JSON;
import com.jplus.core.utill.JSON.JSONArray;
import com.jplus.core.utill.JSON.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.utill.StreamUtil;

public class HttpProxyTest {
	static Logger log = LoggerFactory.getLogger(HttpProxyTest.class);

	public static void main(String[] args) throws IOException {
		HttpClientUtil http = new HttpClientUtil();
		// http://118.24.52.95

		JSONArray jarr = JSON.parseArray(new String(StreamUtil.toReadFile(new File("/config/ip.json"))));
		for (int i = 0; i < jarr.size(); i++) {
			JSONObject jobj = jarr.getJSONObject(i);
			System.out.println(jobj);
			String proxy[] = jobj.getString("proxy").split(":");
			try {
				http.addProxy(proxy[0], Integer.parseInt(proxy[1]));
				String url = "https://clicli.ink/jplus";
				System.out.println(http.doGet(url));
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}

		}
	}
}
