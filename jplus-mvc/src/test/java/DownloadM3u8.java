import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

import com.jplus.core.utill.HttpClientUtil;
import com.jplus.core.utill.JUtil;
import com.jplus.core.utill.HttpClientUtil.RequestMethod;

/**
 * DownloadM3u8
 */
public class DownloadM3u8 {
    static HttpClientUtil http = new HttpClientUtil();
    static char FP = File.separatorChar;
    static char UP = '/';
    static String tmpdir = System.getProperty("java.io.tmpdir");
    static BlockingQueue<String[]> queue = new LinkedBlockingDeque<>();
    static List<String[]> mvlist = new ArrayList<>();
    static ExecutorService pool = Executors.newFixedThreadPool(9);// 下载线程数
    static String filePath = "D:/m3u8/";// 保存目录
    static Boolean delTmp = true;// 自动删除临时文件
    static {
        filePath = "D:/m3u8/";// 保存目录
        // 文件名，下载地址
        mvlist.add(new String[] { "古风教学", "https://ycdncn0l.weilekangnet.com:59666/data6/16043267D65560FD/2FA8E972D1E50F81/index.m3u8" });
        // mvlist.add(new String[] { "庆余年29", "https://mei.huazuida.com/20191216/19522_d95e5001/1000k/hls/index.m3u8" });
        // mvlist.add(new String[] { "庆余年30", "https://mei.huazuida.com/20191216/19533_b0b20b6e/1000k/hls/index.m3u8" });
        // mvlist.add(new String[] { "庆余年31", "https://mei.huazuida.com/20191216/19532_beab241f/1000k/hls/index.m3u8" });
       
    }

    public static void main(final String[] args) throws InterruptedException {
        for (String[] str : mvlist) {
            LOG("开始下载:{}",str[0]);
            String fileName = str[0];
            String m3u8url = str[1];
            downloadM3u8(fileName, m3u8url);// 下载m3u8格式
        }
        pool.shutdownNow();
    }

    private static void downloadM3u8(String fileName, String m3u8url) throws InterruptedException {
        final int x = m3u8url.lastIndexOf("/");
        final String p1 = m3u8url.substring(0, x);
        final String movie[] = http.doGet(m3u8url).split("\n");
        final List<String> mvs = new ArrayList<>();
        int idx = 0;
        for (final String mv : movie) {
            if (!mv.startsWith("#")) {
                mvs.add(mv);
                queue.add(new String[] { String.valueOf(++idx), mv });
            }
        }
        final Integer count = mvs.size();
        final File path = new File(tmpdir + FP + fileName);
        LOG(path.getPath() + "\n" + mvs.size());
        if (!path.exists())
            path.mkdirs();
        final CountDownLatch latch = new CountDownLatch(count);
        final Progressbar bar = new Progressbar();
        final Thread async = new Thread() {
            @Override
            public void run() {
                try {
                    while (true) {
                        final String[] obj = queue.take();
                        final Integer index = Integer.valueOf(obj[0]);
                        final String mv = obj[1];
                        final String url = p1 + UP + mv;
                        pool.execute(new Thread() {
                            @Override
                            public void run() {
                                try {
                                    File f = new File(path, mv);
                                    if (!f.exists())
                                        http.doDownload(url, null, RequestMethod.GET, new File(path, mv));
                                    Double d = 1.00 * index / count * 100.00;
                                    int v = d.intValue();
                                    bar.progress(v, JUtil.formatParams("进度{}%,[{}/{}]:{}", v, index, count, mv));
                                    latch.countDown();
                                } catch (final Exception e) {
                                    queue.add(obj);
                                }
                            }
                        });
                    }
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            }
        };
        async.setDaemon(true);
        async.start();
        latch.await();
        bar.progress(100, "进度100%，下载完成" + JUtil.getPadStr(30, " "));
        // ===============
        List<File> files = new ArrayList<>();
        for (String mv : mvs)
            files.add(new File(path, mv));
        File resultFile = new File(filePath + FP + fileName + ".mpg");
        boolean bo = mergeFiles(files, resultFile);
        String okfile = resultFile.toString();
        LOG("\nDownload state:{}\tpath:{}", bo, okfile);
    }

    public static boolean mergeFiles(final List<File> files, final File resultFile) {
        try {
            resultFile.getParentFile().mkdirs();
            resultFile.delete();
            // resultFile.createNewFile();
            final FileChannel resultFileChannel = new FileOutputStream(resultFile, true).getChannel();
            for (int i = 0; i < files.size(); i++) {
                final FileChannel blk = new FileInputStream(files.get(i)).getChannel();
                resultFileChannel.transferFrom(blk, resultFileChannel.size(), blk.size());
                blk.close();
            }
            resultFileChannel.close();
        } catch (final Exception e) {
            e.printStackTrace();
            return false;
        }
        if (delTmp)
            for (File file : files)
                file.deleteOnExit();
        return true;
    }

    private static void LOG(String log, Object... params) {
        System.out.println(JUtil.formatParams(log, params));
    }

    public static class Progressbar {

        private int lastLen = 0;

        public synchronized void progress(int val, String remark) {
            if (lastLen > 0)
                System.out.print(JUtil.getPadStr(lastLen, "\b"));
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(JUtil.getPadStr(val, ">"));
            sb.append(JUtil.getPadStr(100 - val, " "));
            sb.append("]");
            sb.append(remark);
            lastLen = sb.length() + 3;
            System.out.print(sb.toString());
        }
    }
}