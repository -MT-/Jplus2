package task;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.jplus.core.db.JdbcTemplate;
import com.jplus.core.utill.SimpleSql;
import com.jplus.core.utill.StreamUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * UserBUgFix
 * 
 * @version 1.0 已经没bug了
 */
@Deprecated
public class UserBugFix {
    static Logger log = LoggerFactory.getLogger(UserBugFix.class);

    Properties prop = new Properties();
    JdbcTemplate saas;
    JdbcTemplate lock;
    SimpleSql sql;

    void init() throws FileNotFoundException, IOException, ClassNotFoundException {
        prop.load(new FileInputStream(new File("D:/config/LY_PRD_JDBC.properties")));
        saas = new JdbcTemplate(GET(prop, "saas.jdbcurl"), GET(prop, "saas.user"), GET(prop, "saas.pass"));
        lock = new JdbcTemplate(GET(prop, "lock.jdbcurl"), GET(prop, "lock.user"), GET(prop, "lock.pass"));
        sql = new SimpleSql("simple.sql");
    }

    public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException, IOException {
        UserBugFix fix = new UserBugFix();
        fix.init();
        fix.getRepeat();
    }

    void getRepeat() {
        List<String> repeatUUID = new ArrayList<>();
        List<Map<String, Object>> listRU = lock.queryList(sql.get("queryLookUserRepeat"));
        log.info("ALL:" + listRU.size());
        int size = listRU.size();
        int i = 0;
        for (Map<String, Object> map : listRU) {
            String uuid = (String) map.get("uuid");
            Map<String, Object> saasUser = saas.query(sql.get("querySaasUserByUUID"), uuid);
            log.info("[{}/{}]saasUser>>{}", ++i, size, saasUser);
            if (saasUser == null)
                repeatUUID.add(uuid);
        }
        StringBuilder sb = new StringBuilder();
        for (String uuid : repeatUUID)
            sb.append("'" + uuid + "',\n");
        StreamUtil.toWriteFile(new File("D:/RepeatUUID.txt"), sb.toString().getBytes());
    }

    String GET(Properties prop, String key) {
        return prop.getProperty(key).trim();
    }
}