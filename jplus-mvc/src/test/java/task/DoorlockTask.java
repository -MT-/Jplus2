package task;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.jplus.core.db.JdbcTemplate;
import com.jplus.core.plugin.scheduling.cron.ScheduledTask;
import com.jplus.core.utill.HttpClientUtil;
import com.jplus.core.utill.JSON;
import com.jplus.core.utill.SimpleSql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DoorlockTask
 */
public class DoorlockTask {
    static Logger log = LoggerFactory.getLogger(DoorlockTask.class);

    Properties prop = new Properties();
    JdbcTemplate lock;
    SimpleSql sql;

    void init() throws FileNotFoundException, IOException, ClassNotFoundException {
        prop.load(new FileInputStream(new File("D:/config/LY_PRD_JDBC.properties")));
        lock = new JdbcTemplate(GET(prop, "lock.jdbcurl"), GET(prop, "lock.user"), GET(prop, "lock.pass"));
        sql = new SimpleSql("simple.sql");
    }

    public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException, IOException {
        final DoorlockTask task = new DoorlockTask();
        task.init();
        task.pushTime();
        new ScheduledTask("0 0 6 */1 * ?") {// 每天6点执行
            @Override
            public void invokeMethod() {
                task.pushTime();
            }
        }.start();
    }

    /**
     * 添加：校时待办任务
     */
    void pushTime() {
        List<Map<String, Object>> listTime = lock.queryList(sql.get("listTimeTask"));
        if (!listTime.isEmpty()) {
            log.info("ALL :{}", listTime.size());
            List<Task> listTask = new ArrayList<>();
            for (int i = 0; i < listTime.size(); i++) {
                Map<String, Object> en = listTime.get(i);
                listTask.add(new Task().TIME(en.get("lockMac").toString()));
                if (i % 10 == 0)
                    addTask(listTask);
            }
            addTask(listTask);
        }
    }

    void addTask(List<Task> listTask) {
        HttpClientUtil http = new HttpClientUtil();
        http.setHeader("Content-Type", "application/json");
        String host = GET(prop, "task.hosturl");
        String url = host + "/api/1/1/door/lock/task/add";
        String param = JSON.toJSONString(listTask);
        log.info("Param:" + param);
        String res = http.doPost(url, param);
        log.info("Result:" + res);
        listTask.clear();
    }

    String GET(Properties prop, String key) {
        return prop.getProperty(key).trim();
    }

    public static class Task {
        public Integer merchantId;
        public Integer userId;
        public String lockMac;
        public Integer taskType;// 1.删除密码，2，获取日志，3.校时
        public Integer taskOrder;
        public String remark;
        public Integer pwdIndex;
        public String password;

        public Task() {
        }

        // 校时
        public Task TIME(String lockMac) {
            this.merchantId = 1;
            this.userId = 2;
            this.taskType = 3;
            this.lockMac = lockMac;
            this.taskOrder = 1;
            this.remark = "校时";
            return this;
        }

    }
}