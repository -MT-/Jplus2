package com.jplus.mvc.action.api.bean;

import java.util.Date;

public class SysAreaCode {
	public int id;// bigint(11) NOT NULL
	public int area_code;// int(6) NOT NULL
	public String area_name;// varchar(32) NULL
	public int pid;// int(6) NULL
	public boolean state;// tinyint(1) NULL
	public String remark;// varchar(32) NULL
	public Date create_time;// datetime NULL
}
