package com.jplus.mvc.action.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.anno.com.Autowired;
import com.jplus.core.mvc.anno.RequestMapping;
import com.jplus.core.mvc.anno.RequestParam;
import com.jplus.core.mvc.anno.RestController;
import com.jplus.core.mvc.reqeust.RequestMethod;
import com.jplus.core.mvc.vo.Model;
import com.jplus.core.utill.Assert;
import com.jplus.core.utill.JUtil;
import com.jplus.mvc.action.api.bean.IDCardAuthVo;
import com.jplus.mvc.action.api.bean.IDCardOCRVo;
import com.jplus.mvc.action.api.db.DBStore;
import com.jplus.mvc.action.api.kit.Global;
import com.jplus.mvc.action.api.kit.IDCardKit.IDCard;
import com.jplus.mvc.action.api.kit.IDCardKit.STATUS;
import com.jplus.mvc.action.api.service.CommonService;

@RestController
public class AuthenAction {
	private final Logger log = LoggerFactory.getLogger(AuthenAction.class);
	private @Autowired DBStore dBStore;
	private @Autowired CommonService com;

	/**
	 * 二要素实名认证
	 */
	@RequestMapping(value = "/api/idcard/authen2", method = RequestMethod.POST)
	public Model authen2(@RequestParam("cardNo") String cardNo, @RequestParam("name") String name) {
		Assert.isTrue(JUtil.isNotEmpty(cardNo) && JUtil.isNotEmpty(name), "卡号与姓名不能为空");
		// == 开始认证 ========
		IDCard card = Global.IDCARDKIT.doCheck(cardNo);
		if (card.status == STATUS.SUCC) {// 基础校验通过
			boolean bo = com.authenCard(cardNo, name);
			return new Model().SUCC().DATA(new IDCardAuthVo(true, name, card, bo));// 返回成功/失败;
		}
		return new Model().SUCC().DATA(new IDCardAuthVo(false, name, card, false));// 返回失败;
	}

	/**
	 * 身份证图片OCR+二要素实名认证
	 */
	@RequestMapping(value = "/api/idcard/authen/ocr", method = RequestMethod.POST)
	public Model authenocr(@RequestParam("image") String imgBase64) {
		if (imgBase64.startsWith("data:image")) {
			imgBase64 = imgBase64.substring(imgBase64.indexOf(',') + 1);
			IDCardOCRVo vo = com.cardOcr(imgBase64);
			if (vo != null) {
				String cardNo = vo.cardNo;
				String name = vo.name;
				// == 开始认证 ========
				IDCard card = Global.IDCARDKIT.doCheck(cardNo);
				if (card.status == STATUS.SUCC) {// 基础校验通过
					boolean bo = com.authenCard(cardNo, name);
					return new Model().SUCC().DATA(new IDCardAuthVo(true, name, card, bo));// 返回成功/失败;
				}
				return new Model().SUCC().DATA(new IDCardAuthVo(false, name, card, false));// 返回失败;
			}
		}
		return new Model().FAIL().DATA("身份证图片不满足要求");
	}
}