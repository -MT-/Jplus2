package com.jplus.mvc.action.api.bean;

public class IDCardOCRVo {
    public String name;// 姓名
    public String nation;// 民族
    public String address;// 住址
    public String cardNo;// 公民身份号码
    public String birth;// 出生
    public String sex;// 性别

    public IDCardOCRVo(String name, String nation, String address, String cardNo, String birth, String sex) {
        this.name = name;
        this.nation = nation;
        this.address = address;
        this.cardNo = cardNo;
        this.birth = birth;
        this.sex = sex;
    }

    public IDCardOCRVo() {
    }

}