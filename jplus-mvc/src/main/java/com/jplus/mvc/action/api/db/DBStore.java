package com.jplus.mvc.action.api.db;

import com.jplus.core.anno.boot.Component;
import com.jplus.core.anno.com.Autowired;
import com.jplus.core.db.JdbcTemplate;
import com.jplus.mvc.action.api.bean.SysIDCard;

@Component
public class DBStore {

	private @Autowired JdbcTemplate jdbc;

	public SysIDCard getIDCard(String cardNo, String name) {
		return jdbc.queryBean("SELECT * FROM sys_id_card WHERE card_code=? and card_name=? LIMIT 1; ", SysIDCard.class,
				cardNo, name);
	}

	public SysIDCard getIDCard(String cardNo) {
		return jdbc.queryBean("SELECT * FROM sys_id_card WHERE card_code=?  LIMIT 1 ", SysIDCard.class, cardNo);
	}

	public boolean addIDCard(SysIDCard en) {
		return jdbc.execute("INSERT INTO  sys_id_card (card_code,card_name,area_code,sex,age) VALUES(?,?,?,?,?)",
				en.card_code, en.card_name, en.area_code, en.sex, en.age);
	}

}
