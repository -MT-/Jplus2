
package com.jplus.mvc.action.doorlock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RandomSequence {

	public static void main(String[] args) {
		int seed = 100000019;// 种子
		int count = 87616;
		// int count = 87616;
		RandomSequence r = new RandomSequence(seed);
		System.out.println(r.getSequence(count).size());
		System.err.println(r.getSequence().get(42779));
		
	}

	private long seed;
	private static final long multiplier = 0x5DEECE66DL;
	private static final long addend = 0xBL;
	private List<Integer> sequence = new ArrayList<>();

	public RandomSequence(int seed) {
		this.seed = seed;
	}

	public List<Integer> getSequence() {
		return sequence;
	}

	public List<Integer> getSequence(Integer count) {
		Integer[] seq = new Integer[64];
		int a = 64, b = count / a, c = 0, d = 10, e = 0;
		for (; e < d; e++) {
			for (int i = 1 + e; i < b + 1; i += d) {
				for (; c < a; c++) {
					seq[c] = i + b * c;
				}
				c = 0;
				// ========================
				int no = seq.length;
				for (int y = 0; y < no; y++) {
					int p = nextInt(no);
					if (p == y) {
						y--;
						continue;
					}
					seq[y] ^= seq[p];
					seq[p] ^= seq[y];
					seq[y] ^= seq[p];
				}
				// ========================
				sequence.addAll(Arrays.asList(seq));
//				System.out.println(seq.length + ":" + Arrays.asList(seq));
			}
		}
		return sequence;
	}

	private int nextInt(int n) {
		if (n <= 0)
			throw new IllegalArgumentException("n must be positive");

		int bits, val;
		do {
			bits = next(31);
			val = bits % n;// 考虑离散性
		} while (bits - val + (n - 1) < 0);
		return val;
	}

	private int next(int bits) {
		seed = (seed * multiplier + addend);
		return (int) seed;
	}

}
