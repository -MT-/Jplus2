package com.jplus.mvc.action.api.bean;

import com.jplus.mvc.action.api.kit.IDCardKit.IDCard;

public class IDCardAuthVo {

    public boolean expense;// 是否消费次数
    public String name;// 姓名
    public IDCard idCard;// 实名内容
    public boolean authen;// 实名认证结果
    public String remark;//

    public IDCardAuthVo() {
    }

    public IDCardAuthVo(boolean expense, String name, IDCard idCard, boolean authen) {
        this.name = name;
        this.expense = expense;
        this.idCard = idCard;
        this.authen = authen;
        this.remark = authen ? "实名认证通过" : "实名认证不通过";
    }

}