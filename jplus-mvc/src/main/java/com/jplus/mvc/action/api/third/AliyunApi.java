package com.jplus.mvc.action.api.third;

import java.util.HashMap;
import java.util.Map;

import com.jplus.core.anno.boot.Component;
import com.jplus.core.utill.HttpClientUtil;
import com.jplus.core.utill.JSON;
import com.jplus.core.utill.JSON.JSONObject;
import com.jplus.core.utill.JUtil;
import com.jplus.mvc.action.api.kit.Global;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class AliyunApi {
	private static Logger log = LoggerFactory.getLogger(AliyunApi.class);
	private static HttpClientUtil http = new HttpClientUtil();

	/**
	 * 银行卡 识别
	 * 
	 * @param bankCardNo
	 */
	public Map<String, Object> bankCheck(String bankCardNo) {
		bankCardNo = JUtil.trimAll(bankCardNo);
		// 卡号校验
		String url_check = "https://ccdcapi.alipay.com/validateAndCacheCardInfo.json?_input_charset=utf-8&cardNo=%s&cardBinCheck=true";
		// 银行图片
		// String url_img =
		// "https://apimg.alipay.com/combo.png?d=cashier&t=%s";//%s=bankCode
		JSONObject jobj = JSON.parseObject(http.doGet(String.format(url_check, bankCardNo)));
		if (jobj.getBoolean("validated")) {// true 校验通过
			Map<String, Object> vo = new HashMap<>();
			vo.put("cardNo", bankCardNo);
			vo.put("cardType", Global.CARDTYPE.getString(jobj.getString("cardType")));
			vo.put("bankName", Global.BANKJSON.getString(jobj.getString("bank")));
			vo.put("bankCode", jobj.getString("bank"));
			vo.put("remark", "校验通过");
			return vo;
		}
		return null;
	}

	// AppKey：203750781    
	// AppSecret：110izsfnpvv6c4pjq69a8ddwaf57d2gr
	// AppCode：379d21817ee6419aa47a90dbe58ca24e

	// AppKey：203750777
	// AppSecret：k0niv95l33kk941r7t5i8zw7g40zidow 
	// AppCode：d797501508bf4116ade586c07e3f2f2b

	// AppKey：203750778    
	// AppSecret：vhpwusku0exz43n468yeirteywrc6kg3
	// AppCode：c8993753415e4b2d9d6fa6a968576b19

	// AppKey：23700859
	// AppSecret：235b9b158a01aa65bf811d9531ca6e02
	// AppCode：ae5651c84ba7429b9759367ecd1cbee5

	/**
	 * 实名认证 二要素
	 */
	public boolean idCardAuth(String cardNo, String realName) {
		String ali_host = "http://idcard.market.alicloudapi.com/lianzhuo/idcard";
		String appcode = "ce603cc7ee504458994eb3c959a98305";
		// 最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
		http.setHeader("Authorization", "APPCODE " + appcode);
		Map<String, Object> param = new HashMap<>();
		param.put("cardno", cardNo);
		param.put("name", realName);
		String res = http.doGet(ali_host, param);
		log.info("idCardAuth:" + res);
		// =========================================================
		JSONObject json = JSON.parseObject(res);
		Integer ok = json.getJSONObject("resp").getInt("code");
		return ok == 0;
	}
}