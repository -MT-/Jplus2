package com.jplus.mvc.action.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jplus.core.anno.com.Autowired;
import com.jplus.core.mvc.anno.RequestMapping;
import com.jplus.core.mvc.anno.RequestParam;
import com.jplus.core.mvc.anno.RestController;
import com.jplus.core.mvc.reqeust.RequestMethod;
import com.jplus.core.mvc.vo.Model;
import com.jplus.core.utill.Assert;
import com.jplus.core.utill.JUtil;
import com.jplus.mvc.action.api.bean.IDCardOCRVo;
import com.jplus.mvc.action.api.kit.Global;
import com.jplus.mvc.action.api.kit.IDCardKit;
import com.jplus.mvc.action.api.kit.IDCardKit.IDCard;
import com.jplus.mvc.action.api.service.CommonService;
import com.jplus.mvc.action.api.third.AliyunApi;
import com.jplus.mvc.action.api.third.BaiduApi;

@RestController
public class FreeAction {
	// private final Logger log = LoggerFactory.getLogger(FreeAction.class);
	private @Autowired BaiduApi baidu;
	private @Autowired AliyunApi aliyun;
	private @Autowired Global global;
	private @Autowired CommonService com;

	/**
	 * 校验身份证号
	 */
	@RequestMapping("/free/idcard/check")
	public Model idCardCheck(@RequestParam("cardNos") String cardNos) {
		Assert.isTrue(JUtil.isNotEmpty(cardNos), "卡号不能为空");
		List<String> list = new ArrayList<>();
		char[] cs = cardNos.toCharArray();
		int i = 1;
		StringBuilder sb = new StringBuilder(18);
		for (char c : cs) {
			if ((c >= '0' && c <= '9') || c == 'X' || c == 'x') {
				sb.append(c);
				if (i++ % 18 == 0) {
					list.add(sb.toString());
					sb.delete(0, sb.length());
				}
			}
		}
		// =========================
		List<IDCard> result = new ArrayList<IDCardKit.IDCard>();
		for (String card : list)
			result.add(Global.IDCARDKIT.doCheck(card));
		return new Model().SUCC().DATA(result);
	}

	/**
	 * 构建身份证号
	 */
	@RequestMapping("/free/idcard/build")
	public Model build(@RequestParam("areaName") String areaName, @RequestParam("birthday") String birthday,
			@RequestParam("sex") String sex) {
		Assert.isTrue(JUtil.isNotEmpty(areaName) && JUtil.isNotEmpty(birthday), "地区与出生年月日不能为空");
		Integer areaCode = 0;
		for (String[] var : Global.IDCARDKIT.listArea()) {
			String name = var[1];
			if (name.contains(areaName)) {
				areaCode = Integer.valueOf(var[0]);
				break;
			}
		}
		List<IDCard> list = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			list.add(Global.IDCARDKIT.doBuild(areaCode.toString(), birthday, sex.equals("男")));
		}
		return new Model().SUCC().DATA(list);
	}

	/**
	 * 身份证图片OCR
	 */
	@RequestMapping(value = "/free/idcard/ocr", method = RequestMethod.POST)
	public Model idcardocr(@RequestParam("image") String imgBase64) {
		if (imgBase64.startsWith("data:image")) {
			imgBase64 = imgBase64.substring(imgBase64.indexOf(',') + 1);
			IDCardOCRVo vo = com.cardOcr(imgBase64);
			if (vo != null)
				return new Model().SUCC().DATA(vo);
		}
		return new Model().FAIL().DATA("身份证图片不满足要求");
	}

	/**
	 * 银行卡图片OCR
	 */
	@RequestMapping(value = "/free/bank/ocr", method = RequestMethod.POST)
	public Model bankocr(@RequestParam("image") String imgBase64) {
		if (imgBase64.startsWith("data:image")) {
			imgBase64 = imgBase64.substring(imgBase64.indexOf(',') + 1);
			String bankCardNo = com.bankOcr(imgBase64);
			if (bankCardNo != null) {
				Map<String, Object> map = aliyun.bankCheck(bankCardNo);
				if (map != null)
					return new Model().SUCC().DATA(map);
			}
			return new Model().FAIL().DATA("图片未识别出银行卡信息");
		}
		return new Model().FAIL().DATA("银行卡图片不满足要求");
	}

	/**
	 * 银行卡bin检查
	 */
	@RequestMapping("/free/bank/check")
	public Model bankcheck(@RequestParam("bankCardNo") String bankCardNo) {
		Assert.isTrue(JUtil.isNotEmpty(bankCardNo), "银行卡号不能为空");
		Map<String, Object> map = aliyun.bankCheck(bankCardNo);
		if (map != null)
			return new Model().SUCC().DATA(map);
		return new Model().FAIL().DATA("校验失败，请输入正确的银行卡号~");
	}

}
