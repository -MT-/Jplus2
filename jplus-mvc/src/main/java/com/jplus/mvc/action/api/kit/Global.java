package com.jplus.mvc.action.api.kit;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.PostConstruct;

import com.jplus.core.anno.boot.Component;
import com.jplus.core.anno.com.Autowired;
import com.jplus.core.anno.com.Value;
import com.jplus.core.plugin.async.Async;
import com.jplus.core.utill.JSON;
import com.jplus.core.utill.JSON.JSONObject;
import com.jplus.core.utill.StreamUtil;
import com.jplus.mvc.action.api.db.DBStore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 全局
 */
@Component
public class Global {
	private Logger log = LoggerFactory.getLogger(Global.class);

	public static final IDCardKit IDCARDKIT = new IDCardKit();// 身份证工具类
	public static final JSONObject BANKJSON = new JSONObject();// 银行卡所属行
	public static final JSONObject CARDTYPE = new JSONObject();// 银行卡类型

	private @Autowired DBStore dBStore;
	private @Value("${files.img.card}") String cardPath;

	@PostConstruct
	public void init() throws IOException {
		// 0======
		CARDTYPE.put("DC", "储蓄卡");
		CARDTYPE.put("CC", "信用卡");
		CARDTYPE.put("SCC", "准贷记卡");
		CARDTYPE.put("PC", "预付费卡");
		InputStream in = ClassLoader.getSystemResourceAsStream("data/bank.json");// 读jar包根目录下的文件
		BANKJSON.putAll(JSON.parseObject(new String(StreamUtil.toByteArray(in))));
		log.info("[init]>> load bank.json：" + BANKJSON.size());
	}

	/**
	 * 异步存储
	 */
	@Async("fixedThreadPool")
	public void saveCardImg(String fileName, byte[] bytes) {
		log.info(":异步存储文件:" + fileName);
		String APPEND = fileName.substring(0, 6);// 文件夹
		File file = new File(cardPath + "/" + APPEND + "/" + fileName);
		StreamUtil.toWriteFile(file, bytes);
	}
}