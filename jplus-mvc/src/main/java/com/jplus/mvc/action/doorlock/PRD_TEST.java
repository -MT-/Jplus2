package com.jplus.mvc.action.doorlock;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.jplus.core.utill.JSON;
import com.jplus.core.utill.JUtil;
import com.jplus.core.utill.StreamUtil;
import com.jplus.mvc.action.doorlock.DoorLockAction.InitLock;
import com.jplus.mvc.action.doorlock.DoorLockAction.Pwd;

/**
 * PRD_TEST
 */
public class PRD_TEST {
    static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

    public static void main(String[] args) throws ParseException {
        // SELECT
        // a.lockId,a.lockMac,a.`highSeed`,a.`lowSeed`,a.`initTime`,b.`lockRootKey` FROM
        // `t_door_lock_random_cipher` a
        // LEFT JOIN `t_door_lock_secret_key` b ON a.`lockId`=b.`lockId`
        // WHERE a.`status`=1 AND b.`status`=1
        readFile(new File("D://DoorLockKEY.csv"));
    }

    private static void readFile(File fileRes) throws ParseException {
        List<String> csvs = StreamUtil.toReadFileLine(fileRes);
        int i = 0;
        int a = csvs.size();
        StringBuilder sb = new StringBuilder();
        String startTime = "2020-03-09T01:00";
        String endTime = "2020-03-15T00:00";
        for (String c : csvs) {
            i++;
            String[] cs = c.split(",");
            if (!JUtil.isNumber(cs[2]))
                continue;

            InitLock lock = new InitLock();
            lock.seed1 = JUtil.toInt(cs[2]);
            lock.seed2 = JUtil.toInt(cs[3]);
            lock.initTime = sdf.format(new Date(Long.parseLong(cs[4]) * 3600L * 1000L));
            lock.key = cs[5].trim();
            System.err.println(JSON.toJSONString(lock));
            initLock(lock);// 初始化锁
            Pwd pwd = new Pwd();
            pwd.startTime = startTime;
            pwd.endTime = endTime;
            pwd.type = 1;
            buildPwd(pwd, lock);
            System.out.println(i + "/" + a + JSON.toJSONString(pwd));
            sb.append(cs[0] + "," + cs[1] + "," + pwd.aesPwd + "\n");
        }
        StreamUtil.toWriteFile(new File("D://密码0309.txt"), sb.toString().getBytes());
    }

    private static InitLock initLock(InitLock lock) {
        int seed1 = lock.seed1;// 种子1
        int count1 = 87616;//
        RandomSequence seq1 = new RandomSequence(seed1);
        lock.sequenceHigh = seq1.getSequence(count1);

        int seed2 = lock.seed2;// 种子2
        int count2 = 97536;
        RandomSequence seq2 = new RandomSequence(seed2);
        lock.sequenceLow = seq2.getSequence(count2);
        return lock;
    }

    private static Pwd buildPwd(Pwd pwd, InitLock lock) throws ParseException {
        pwd.startHour = RandomCipherHelper.getLocalTimeHour(sdf.parse(pwd.startTime.substring(0, 16)));
        pwd.endHour = RandomCipherHelper.getLocalTimeHour(sdf.parse(pwd.endTime.substring(0, 16)));
        // == 生成密码 =======================================
        String pwdStart = RandomCipherHelper.computHighPwd(lock.sequenceHigh, lock.getInitHour(), pwd.startHour);
        List<Integer> pwdends = RandomCipherHelper.computLowPwd(lock.sequenceLow, pwd.startHour, pwd.endHour, pwd.type,
                pwd.loopStrategy, pwd.loopEndPoint);
        int index = (int) (Math.random() * pwdends.size());// 随便取一个
        Integer pwdEnd = pwdends.get(index);
        String pwds = pwdStart + "" + pwdEnd;

        // == 密码加密 =======================================
        AES10 aes10 = new AES10(lock.key.toCharArray(), lock.key.toCharArray());
        String aesPwd = new String(aes10.Encrypt(pwds.toCharArray(), pwds.length()));// 加密后
        String radPwd = new String(aes10.Decrypt(aesPwd.toCharArray(), aesPwd.length()));// 解密后
        // ==
        pwd.aesPwd = aesPwd;
        pwd.radPwd = radPwd;
        return (pwd);
    }


}