package com.jplus.mvc.action.api.bean;

public class SysIDCard {
    public int id;// bigint(11) NOT NULL
    public String card_code;// varchar(18) NOT NULL
    public String card_name;// varchar(32) NULL
    public int area_code;// int(6) NULL
    public int sex;// tinyint(4) NULL0女 1男
    public int age;// int(8) NULL
    public String create_time;//

    public SysIDCard() {
    }

    public SysIDCard(String card_code, String card_name, int area_code, int sex, int age) {
        this.card_code = card_code;
        this.card_name = card_name;
        this.area_code = area_code;
        this.sex = sex;
        this.age = age;
    }

}