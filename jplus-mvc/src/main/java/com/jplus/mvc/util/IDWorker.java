package com.jplus.mvc.util;

import java.util.ArrayList;
import java.util.List;

import com.jplus.core.utill.JUtil;

/**53
 * 16+32+
 * Snowflake ID组成结构： 正数位（占1比特）+ 时间戳（占41比特）+ 机器ID（占5比特）+ 数据中心（占5比特）+
 * 自增值（占12比特），总共64比特组成的一个Long类型。
 */
public class IDWorker {
    private long workId; // 机器id
    private long dataId; // 数据id
    private long sequence;// 12位的序列号

    private IDWorker(long serviceId) {// serviceId=workId+dataId
        long workId = serviceId >> workIdBits;
        long dataId = workId << workIdBits ^ serviceId;
        init(workId, dataId);
    }

    private IDWorker(long workId, long dataId) {
        init(workId, dataId);
    }

    private void init(long workId, long dataId) {
        System.out.println(String.format("workId: %d，dataId: %d", workId, dataId));
        if (workId > maxworkId || workId < 0)
            throw new IllegalArgumentException(
                    String.format("work Id can't be greater than %d or less than 0", maxworkId));
        if (dataId > maxdataId || dataId < 0)
            throw new IllegalArgumentException(
                    String.format("data Id can't be greater than %d or less than 0", maxdataId));
        this.workId = workId;
        this.dataId = dataId;
        this.sequence = 0;
    }

    // 初始时间戳
    private long twepoch = 0L;// 1577808000000

    // 长度为5位
    private long workIdBits = 5L;
    private long dataIdBits = 5L;

    // 最大值
    private long maxworkId = -1L ^ (-1L << workIdBits);
    private long maxdataId = -1L ^ (-1L << dataIdBits);
    // 序列号id长度
    private long sequenceBits = 12L;
    // 序列号最大值
    private long sequenceMask = -1L ^ (-1L << sequenceBits);

    // 工作id需要左移的位数，12位
    private long workIdShift = sequenceBits;
    // 数据id需要左移位数 12+5=17位
    private long dataIdShift = sequenceBits + workIdBits;
    // 时间戳需要左移位数 12+5+5=22位
    private long timestampLeftShift = sequenceBits + workIdBits + dataIdBits;

    // 上次时间戳，初始值为负数
    private long lastTimestamp = -1L;

    public synchronized long nextId() {
        long timestamp = timeGen();
        // 获取当前时间戳如果小于上次时间戳，则表示时间戳获取出现异常
        if (timestamp < lastTimestamp)
            throw new RuntimeException(String.format(
                    "Clock moved backwards. Refusing to generate id for %d milliseconds", lastTimestamp - timestamp));
        // 获取当前时间戳如果等于上次时间戳（同一毫秒内），则在序列号加一；否则序列号赋值为0，从0开始。
        if (lastTimestamp == timestamp) {
            sequence = (sequence + 1) & sequenceMask;
            if (sequence == 0)
                timestamp = tilNextMillis(lastTimestamp);
        } else {
            sequence = 0;
        }
        // 将上次时间戳值刷新
        lastTimestamp = timestamp;
        /**
         * 返回结果： (timestamp - twepoch) << timestampLeftShift) 表示将时间戳减去初始时间戳，再左移相应位数
         * (dataId << dataIdShift) 表示将数据id左移相应位数 (workId << workIdShift) 表示将工作id左移相应位数 |
         * 是按位或运算符，例如：x | y，只有当x，y都为0的时候结果才为0，其它情况结果都为1。
         * 因为个部分只有相应位上的值有意义，其它位上都是0，所以将各部分的值进行 | 运算就能得到最终拼接好的id
         */
        return ((timestamp - twepoch) << timestampLeftShift) | (dataId << dataIdShift) | (workId << workIdShift)
                | sequence;
    }

    // 【阻塞 强制过秒】获取时间戳，并与上次时间戳比较
    private long tilNextMillis(long lastTimestamp) {
        long timestamp = timeGen();
        while (timestamp <= lastTimestamp)
            timestamp = timeGen();
        return timestamp;
    }

    // 获取系统时间戳
    private long timeGen() {
        return System.currentTimeMillis();
    }

    private static IDWorker instance;

    public static synchronized IDWorker getInstance(long workId, long dataId) {
        if (instance == null)
            instance = new IDWorker(workId, dataId);
        return instance;
    }

    public static synchronized IDWorker getInstance(long serviceId) {
        if (instance == null)
            instance = new IDWorker(serviceId);
        return instance;
    }

    // ---------------测试---------------
    public static void main(String[] args) throws InterruptedException {
        IDWorker worker = IDWorker.getInstance(1);
        List<Long> set = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            set.add(worker.nextId());
        }
        for (Long t : set) {
            System.out.println(t + "\t" + JUtil.leftPad(UUIDUtil.parseStrJZ(t, 2), 64, "0"));
        }
        System.err.println(set.size());
    }
}