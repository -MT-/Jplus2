package com.jplus.mvc.util;

import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * [Snowflake] 53 or 64 bits unique id
 * 
 * <pre>
 * [FIX:JS安全整数BUG]: type of number's max value < 53bits
 * |00000000|00011111|11111111|11111111|11111111|11111111|11111111|11111111|
 * |--------|---xxxxx|xxxxxxxx|xxxxxxxx|xxxxxxxx|xxx-----|--------|--------|32=时间戳秒
 * |--------|--------|--------|--------|--------|---xxxxx|--------|--------|05=31
 * |--------|--------|--------|--------|--------|--------|xxxxxxxx|xxxxxxxx|16=65535
 * 时间戳[32]+机器节点[5]+自增序列[16]=53bit
 * </pre>
 */
public final class IDUtil {
	private final static Logger LOG=LoggerFactory.getLogger(IDUtil.class);
	private final static int W_BIT = 05;// 可改：机器节点 bit位
	private final static int N_BIT = 16;// 可改：自增序列 bit位
	private final static long startEpoch = 1577808000L;// 可改：起始偏移点 2020/1/1 0:0:0

	private final static int ALL_BIT = N_BIT + W_BIT;
	private final static int WORK_MAX = 1 << W_BIT;
	private final static int NEXT_MAX = 1 << N_BIT;

	private int workId = 0;// 机器节点
	private int nextId = 0;// 自增序列
	private long lastPoint = 0;// 最后时间

	// == INSTANCE =======================================
	private final static Map<Integer, IDUtil> inst = new ConcurrentHashMap<>();

	/**
	 * <pre>
	 * IDUtil kit = new IDUtil(1);
	 * long id = kit.get();
	 * </pre>
	 * 
	 * @param workId 机器节点 id： 0~31
	 */
	public static synchronized IDUtil getInstance(int workId) {
		if (workId >= WORK_MAX)
			throw new RuntimeException("WorkId must be less than " + WORK_MAX);
		if (inst.get(workId) == null)
			inst.put(workId, new IDUtil(workId));
		return inst.get(workId);
	}

	private IDUtil(int workId) {
		this.workId = workId;
	}

	// == CORE ============================================
	public long get() {
		return get(Instant.now().getEpochSecond());
	}

	private synchronized long get(long currEpoch) {
		if (currEpoch <= lastPoint) {
			currEpoch = lastPoint;
		} else {
			lastPoint = currEpoch;
			nextId = 0;
		}
		if (++nextId >= NEXT_MAX){
			LOG.warn("Get ID too fast, please increase the serial number maximum");
			return get(currEpoch + 1);// 抢下一秒
		}
		return ((currEpoch - startEpoch) << ALL_BIT) | (nextId << W_BIT) | workId;
	}

	// == PARSE ============================================
	public static class ID {
		public long time;// utc time
		public int workId;
		public int nextId;

		@Override
		public String toString() {
			return "ID=[nextId=" + nextId + ", workId=" + workId + ", time=" + time + "]";
		}
	}

	public static ID parseId(long id) {
		ID pojo = new ID();
		long time = id >> ALL_BIT;
		pojo.time = (time + startEpoch);
		long tmp = (time << ALL_BIT) ^ id;
		pojo.nextId = (int) (tmp >> W_BIT);
		pojo.workId = (int) ((pojo.nextId << W_BIT) ^ tmp);
		return pojo;
	}

}