package com.jplus.mvc.util;

import com.jplus.core.utill.Assert;

/**
 * UUID生成工具类<br>
 * 支持自定义长度<br>
 * UUID是可重复的，我们要保证的只是尽量不重复。
 * 
 * @author Yuanqy
 */
public class UUIDUtil {
	private final static String chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public static String getUUID() {
		return getCharStr(32, 36);
	}

	/**
	 * @param len 长度
	 * @param jz  进制[2~62]
	 */
	public static String getCharStr(int len, int jz) {
		Assert.isTrue(jz >= 2 && jz <= 62, "param 'jz' must be between 2 and 62");
		String a = "";
		for (int i = 0; i < len; i++)
			a += chars.charAt((int) Math.round(Math.random() * (jz - 1)));
		return a;
	}

	/**
	 * 10进制转化成其他进制
	 */
	public static String parseStrJZ(long num, int jz) {
		String str = "";
		if (num == 0) {
			return "";
		} else {
			str = parseStrJZ(num / jz, jz);
			return str + chars.charAt((int) (num % jz));
		}
	}

	/**
	 * 其他进制转化成10进制
	 */
	public static long parseLong10(String str, int jz) {
		long result = 0;
		int len = str.length();
		for (int i = len; i > 0; i--) {
			result += chars.indexOf(str.charAt(i - 1)) * ((long) (Math.pow(jz, len - i)));
		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(getCharStr(32, 16).toUpperCase());
	}
}
