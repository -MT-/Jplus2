package com.jplus.mvc.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.jplus.core.utill.JUtil;

/**
 * 使用Java自带的MessageDigest类
 */
public class EncryptionUtil {

	/**
	 * @param source   需要加密的字符串
	 * @param hashType 加密类型 （MD5 和 SHA）
	 */
	public static String getHash(String source, String hashType) {
		MessageDigest md5;
		try {
			md5 = MessageDigest.getInstance(hashType);
			md5.update(source.getBytes());
			return JUtil.byte2Hex(md5.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}
}