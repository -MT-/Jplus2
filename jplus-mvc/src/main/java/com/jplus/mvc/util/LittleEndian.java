package com.jplus.mvc.util;

/**
 * LittleEndian 小端格式 Little-Endian: 低地址存放低位
 */
public class LittleEndian {

    public static byte[] get(short value) {
        byte[] var1 = new byte[] { (byte) value, (byte) (value >> 8) };
        return var1;
    }

    public static byte[] get(int value) {
        byte[] var1 = new byte[] { (byte) value, (byte) (value >> 8), (byte) (value >> 16), (byte) (value >> 24) };
        return var1;
    }

    public static byte[] get(long value) {
        byte[] var2 = new byte[] { (byte) ((int) value), (byte) ((int) (value >> 8)), (byte) ((int) (value >> 16)),
                (byte) ((int) (value >> 24)), (byte) ((int) (value >> 32)), (byte) ((int) (value >> 40)),
                (byte) ((int) (value >> 48)), (byte) ((int) (value >> 56)) };
        return var2;
    }

    public static byte get(byte value) {
        return value;
    }
}